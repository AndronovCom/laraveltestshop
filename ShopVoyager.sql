-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Окт 17 2018 г., 08:59
-- Версия сервера: 5.6.38
-- Версия PHP: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `ShopVoyager`
--

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `order`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 'Category 1', 'category-1', '2018-10-17 02:59:03', '2018-10-17 02:59:03'),
(2, NULL, 1, 'Category 2', 'category-2', '2018-10-17 02:59:03', '2018-10-17 02:59:03');

-- --------------------------------------------------------

--
-- Структура таблицы `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '', 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, '', 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, '', 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, '', 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '', 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, '', 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":\"0\"}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'locale', 'text', 'Locale', 0, 1, 1, 1, 1, 0, '', 12),
(12, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, '', 12),
(13, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(14, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '', 2),
(15, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '', 3),
(16, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 4),
(17, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(18, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '', 2),
(19, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '', 3),
(20, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 4),
(21, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, '', 5),
(22, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, '', 9),
(23, 4, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(24, 4, 'parent_id', 'select_dropdown', 'Parent', 0, 0, 1, 1, 1, 1, '{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}', 2),
(25, 4, 'order', 'text', 'Order', 1, 1, 1, 1, 1, 1, '{\"default\":1}', 3),
(26, 4, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '', 4),
(27, 4, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\"}}', 5),
(28, 4, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, '', 6),
(29, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 7),
(30, 5, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(31, 5, 'author_id', 'text', 'Author', 1, 0, 1, 1, 0, 1, '', 2),
(32, 5, 'category_id', 'text', 'Category', 1, 0, 1, 1, 1, 0, '', 3),
(33, 5, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '', 4),
(34, 5, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, '', 5),
(35, 5, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, '', 6),
(36, 5, 'image', 'image', 'Post Image', 0, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 7),
(37, 5, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:posts,slug\"}}', 8),
(38, 5, 'meta_description', 'text_area', 'Meta Description', 1, 0, 1, 1, 1, 1, '', 9),
(39, 5, 'meta_keywords', 'text_area', 'Meta Keywords', 1, 0, 1, 1, 1, 1, '', 10),
(40, 5, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}', 11),
(41, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '', 12),
(42, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 13),
(43, 5, 'seo_title', 'text', 'SEO Title', 0, 1, 1, 1, 1, 1, '', 14),
(44, 5, 'featured', 'checkbox', 'Featured', 1, 1, 1, 1, 1, 1, '', 15),
(45, 6, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(46, 6, 'author_id', 'text', 'Author', 1, 0, 0, 0, 0, 0, '', 2),
(47, 6, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '', 3),
(48, 6, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, '', 4),
(49, 6, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, '', 5),
(50, 6, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\"},\"validation\":{\"rule\":\"unique:pages,slug\"}}', 6),
(51, 6, 'meta_description', 'text', 'Meta Description', 1, 0, 1, 1, 1, 1, '', 7),
(52, 6, 'meta_keywords', 'text', 'Meta Keywords', 1, 0, 1, 1, 1, 1, '', 8),
(53, 6, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}', 9),
(54, 6, 'created_at', 'timestamp', 'Created At', 1, 1, 1, 0, 0, 0, '', 10),
(55, 6, 'updated_at', 'timestamp', 'Updated At', 1, 0, 0, 0, 0, 0, '', 11),
(56, 6, 'image', 'image', 'Page Image', 0, 1, 1, 1, 1, 1, '', 12);

-- --------------------------------------------------------

--
-- Структура таблицы `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', '', '', 1, 0, NULL, '2018-10-17 02:59:03', '2018-10-17 02:59:03'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2018-10-17 02:59:03', '2018-10-17 02:59:03'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, '', '', 1, 0, NULL, '2018-10-17 02:59:03', '2018-10-17 02:59:03'),
(4, 'categories', 'categories', 'Category', 'Categories', 'voyager-categories', 'TCG\\Voyager\\Models\\Category', NULL, '', '', 1, 0, NULL, '2018-10-17 02:59:03', '2018-10-17 02:59:03'),
(5, 'posts', 'posts', 'Post', 'Posts', 'voyager-news', 'TCG\\Voyager\\Models\\Post', 'TCG\\Voyager\\Policies\\PostPolicy', '', '', 1, 0, NULL, '2018-10-17 02:59:04', '2018-10-17 02:59:04'),
(6, 'pages', 'pages', 'Page', 'Pages', 'voyager-file-text', 'TCG\\Voyager\\Models\\Page', NULL, '', '', 1, 0, NULL, '2018-10-17 02:59:04', '2018-10-17 02:59:04');

-- --------------------------------------------------------

--
-- Структура таблицы `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2018-10-17 02:59:03', '2018-10-17 02:59:03');

-- --------------------------------------------------------

--
-- Структура таблицы `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2018-10-17 02:59:03', '2018-10-17 02:59:03', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 5, '2018-10-17 02:59:03', '2018-10-17 02:59:03', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2018-10-17 02:59:03', '2018-10-17 02:59:03', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2018-10-17 02:59:03', '2018-10-17 02:59:03', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 9, '2018-10-17 02:59:03', '2018-10-17 02:59:03', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 10, '2018-10-17 02:59:03', '2018-10-17 02:59:03', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 11, '2018-10-17 02:59:03', '2018-10-17 02:59:03', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 12, '2018-10-17 02:59:03', '2018-10-17 02:59:03', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 13, '2018-10-17 02:59:03', '2018-10-17 02:59:03', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 14, '2018-10-17 02:59:03', '2018-10-17 02:59:03', 'voyager.settings.index', NULL),
(11, 1, 'Categories', '', '_self', 'voyager-categories', NULL, NULL, 8, '2018-10-17 02:59:03', '2018-10-17 02:59:03', 'voyager.categories.index', NULL),
(12, 1, 'Posts', '', '_self', 'voyager-news', NULL, NULL, 6, '2018-10-17 02:59:04', '2018-10-17 02:59:04', 'voyager.posts.index', NULL),
(13, 1, 'Pages', '', '_self', 'voyager-file-text', NULL, NULL, 7, '2018-10-17 02:59:04', '2018-10-17 02:59:04', 'voyager.pages.index', NULL),
(14, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 13, '2018-10-17 02:59:04', '2018-10-17 02:59:04', 'voyager.hooks', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_01_01_000000_create_pages_table', 1),
(6, '2016_01_01_000000_create_posts_table', 1),
(7, '2016_02_15_204651_create_categories_table', 1),
(8, '2016_05_19_173453_create_menu_table', 1),
(9, '2016_10_21_190000_create_roles_table', 1),
(10, '2016_10_21_190000_create_settings_table', 1),
(11, '2016_11_30_135954_create_permission_table', 1),
(12, '2016_11_30_141208_create_permission_role_table', 1),
(13, '2016_12_26_201236_data_types__add__server_side', 1),
(14, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(15, '2017_01_14_005015_create_translations_table', 1),
(16, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(17, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(18, '2017_04_11_000000_alter_post_nullable_fields_table', 1),
(19, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(20, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(21, '2017_08_05_000000_add_group_to_settings_table', 1),
(22, '2017_11_26_013050_add_user_role_relationship', 1),
(23, '2017_11_26_015000_create_user_roles_table', 1),
(24, '2018_03_11_000000_add_user_settings', 1),
(25, '2018_03_14_000000_add_details_to_data_types_table', 1),
(26, '2018_03_16_000000_make_settings_value_nullable', 1),
(27, '2018_10_01_145500_create_shop_clients', 1),
(28, '2018_10_01_145538_create_shop_orders', 1),
(29, '2018_10_01_145621_create_shop_order_items', 1),
(30, '2018_10_01_145656_create_shop_order_files', 1),
(31, '2018_10_01_145735_create_shop_order_add', 1),
(32, '2018_10_01_145810_create_shop_npcity', 1),
(33, '2018_10_01_145843_create_shop_npunit', 1),
(34, '2018_10_01_145912_create_shop_info', 1),
(35, '2018_10_01_145947_create_shop_gallery', 1),
(36, '2018_10_01_150020_create_shop_jobs', 1),
(37, '2018_10_04_155650_create_shop_category_table', 1),
(38, '2018_10_04_160553_create_shop_attr_table', 1),
(39, '2018_10_04_160553_create_shop_attr_val_table', 1),
(40, '2018_10_04_160553_create_shop_category_attr_table', 1),
(41, '2018_10_04_160553_create_shop_comments_table', 1),
(42, '2018_10_04_160553_create_shop_recommends_products_table', 1),
(43, '2018_10_04_162548_create_shop_product_table', 1),
(44, '2018_10_04_193307_foreign_attr_val', 1),
(45, '2018_10_04_193601_foreign_category', 1),
(46, '2018_10_04_193656_foreign_products', 1),
(47, '2018_10_04_193952_foreign_category_attr', 1),
(48, '2018_10_04_205731_foreign_comments', 1),
(49, '2018_10_04_205914_foreign_recommends', 1),
(50, '2018_10_05_091953_create_shop_additional_table', 1),
(51, '2018_10_05_113403_foreign_shop_order_additional', 1),
(52, '2018_10_05_115541_foreign_shop_order_items_producr', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `pages`
--

INSERT INTO `pages` (`id`, `author_id`, `title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'Hello World', 'Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.', '<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', 'pages/page1.jpg', 'hello-world', 'Yar Meta Description', 'Keyword1, Keyword2', 'ACTIVE', '2018-10-17 02:59:04', '2018-10-17 02:59:04');

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2018-10-17 02:59:03', '2018-10-17 02:59:03'),
(2, 'browse_bread', NULL, '2018-10-17 02:59:03', '2018-10-17 02:59:03'),
(3, 'browse_database', NULL, '2018-10-17 02:59:03', '2018-10-17 02:59:03'),
(4, 'browse_media', NULL, '2018-10-17 02:59:03', '2018-10-17 02:59:03'),
(5, 'browse_compass', NULL, '2018-10-17 02:59:03', '2018-10-17 02:59:03'),
(6, 'browse_menus', 'menus', '2018-10-17 02:59:03', '2018-10-17 02:59:03'),
(7, 'read_menus', 'menus', '2018-10-17 02:59:03', '2018-10-17 02:59:03'),
(8, 'edit_menus', 'menus', '2018-10-17 02:59:03', '2018-10-17 02:59:03'),
(9, 'add_menus', 'menus', '2018-10-17 02:59:03', '2018-10-17 02:59:03'),
(10, 'delete_menus', 'menus', '2018-10-17 02:59:03', '2018-10-17 02:59:03'),
(11, 'browse_roles', 'roles', '2018-10-17 02:59:03', '2018-10-17 02:59:03'),
(12, 'read_roles', 'roles', '2018-10-17 02:59:03', '2018-10-17 02:59:03'),
(13, 'edit_roles', 'roles', '2018-10-17 02:59:03', '2018-10-17 02:59:03'),
(14, 'add_roles', 'roles', '2018-10-17 02:59:03', '2018-10-17 02:59:03'),
(15, 'delete_roles', 'roles', '2018-10-17 02:59:03', '2018-10-17 02:59:03'),
(16, 'browse_users', 'users', '2018-10-17 02:59:03', '2018-10-17 02:59:03'),
(17, 'read_users', 'users', '2018-10-17 02:59:03', '2018-10-17 02:59:03'),
(18, 'edit_users', 'users', '2018-10-17 02:59:03', '2018-10-17 02:59:03'),
(19, 'add_users', 'users', '2018-10-17 02:59:03', '2018-10-17 02:59:03'),
(20, 'delete_users', 'users', '2018-10-17 02:59:03', '2018-10-17 02:59:03'),
(21, 'browse_settings', 'settings', '2018-10-17 02:59:03', '2018-10-17 02:59:03'),
(22, 'read_settings', 'settings', '2018-10-17 02:59:03', '2018-10-17 02:59:03'),
(23, 'edit_settings', 'settings', '2018-10-17 02:59:03', '2018-10-17 02:59:03'),
(24, 'add_settings', 'settings', '2018-10-17 02:59:03', '2018-10-17 02:59:03'),
(25, 'delete_settings', 'settings', '2018-10-17 02:59:03', '2018-10-17 02:59:03'),
(26, 'browse_categories', 'categories', '2018-10-17 02:59:03', '2018-10-17 02:59:03'),
(27, 'read_categories', 'categories', '2018-10-17 02:59:03', '2018-10-17 02:59:03'),
(28, 'edit_categories', 'categories', '2018-10-17 02:59:03', '2018-10-17 02:59:03'),
(29, 'add_categories', 'categories', '2018-10-17 02:59:03', '2018-10-17 02:59:03'),
(30, 'delete_categories', 'categories', '2018-10-17 02:59:03', '2018-10-17 02:59:03'),
(31, 'browse_posts', 'posts', '2018-10-17 02:59:04', '2018-10-17 02:59:04'),
(32, 'read_posts', 'posts', '2018-10-17 02:59:04', '2018-10-17 02:59:04'),
(33, 'edit_posts', 'posts', '2018-10-17 02:59:04', '2018-10-17 02:59:04'),
(34, 'add_posts', 'posts', '2018-10-17 02:59:04', '2018-10-17 02:59:04'),
(35, 'delete_posts', 'posts', '2018-10-17 02:59:04', '2018-10-17 02:59:04'),
(36, 'browse_pages', 'pages', '2018-10-17 02:59:04', '2018-10-17 02:59:04'),
(37, 'read_pages', 'pages', '2018-10-17 02:59:04', '2018-10-17 02:59:04'),
(38, 'edit_pages', 'pages', '2018-10-17 02:59:04', '2018-10-17 02:59:04'),
(39, 'add_pages', 'pages', '2018-10-17 02:59:04', '2018-10-17 02:59:04'),
(40, 'delete_pages', 'pages', '2018-10-17 02:59:04', '2018-10-17 02:59:04'),
(41, 'browse_hooks', NULL, '2018-10-17 02:59:04', '2018-10-17 02:59:04');

-- --------------------------------------------------------

--
-- Структура таблицы `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `posts`
--

INSERT INTO `posts` (`id`, `author_id`, `category_id`, `title`, `seo_title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `featured`, `created_at`, `updated_at`) VALUES
(1, 0, NULL, 'Lorem Ipsum Post', NULL, 'This is the excerpt for the Lorem Ipsum Post', '<p>This is the body of the lorem ipsum post</p>', 'posts/post1.jpg', 'lorem-ipsum-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2018-10-17 02:59:04', '2018-10-17 02:59:04'),
(2, 0, NULL, 'My Sample Post', NULL, 'This is the excerpt for the sample Post', '<p>This is the body for the sample post, which includes the body.</p>\n                <h2>We can use all kinds of format!</h2>\n                <p>And include a bunch of other stuff.</p>', 'posts/post2.jpg', 'my-sample-post', 'Meta Description for sample post', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2018-10-17 02:59:04', '2018-10-17 02:59:04'),
(3, 0, NULL, 'Latest Post', NULL, 'This is the excerpt for the latest post', '<p>This is the body for the latest post</p>', 'posts/post3.jpg', 'latest-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2018-10-17 02:59:04', '2018-10-17 02:59:04'),
(4, 0, NULL, 'Yarr Post', NULL, 'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.', '<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>', 'posts/post4.jpg', 'yarr-post', 'this be a meta descript', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2018-10-17 02:59:04', '2018-10-17 02:59:04');

-- --------------------------------------------------------

--
-- Структура таблицы `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2018-10-17 02:59:03', '2018-10-17 02:59:03'),
(2, 'user', 'Normal User', '2018-10-17 02:59:03', '2018-10-17 02:59:03');

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', '', '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Voyager', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', '', '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_additional`
--

CREATE TABLE `shop_additional` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `shop_additional`
--

INSERT INTO `shop_additional` (`id`, `name`, `description`, `price`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Dr. Tiara Kertzmann DDS', 'I\'ll set Dinah at.', 0, '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL),
(2, 'Dr. Cristina Davis', 'No, I\'ve made up.', 5, '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL),
(3, 'Dustin Ritchie', 'Alice. \'Come,.', 5, '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL),
(4, 'Kaia Upton', 'I will tell you.', 9, '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL),
(5, 'Chaya Wolf', 'DOTH THE LITTLE.', 3, '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL),
(6, 'Miss Harmony Torphy', 'I shall have to.', 0, '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL),
(7, 'Assunta Wilderman', 'Alice thought she.', 1, '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL),
(8, 'Jess Davis Jr.', 'Alice. \'You are,\'.', 3, '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL),
(9, 'Reid Zboncak DVM', 'For some minutes.', 5, '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL),
(10, 'Litzy Herman', 'Duchess was VERY.', 9, '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_attr`
--

CREATE TABLE `shop_attr` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unit` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `shop_attr`
--

INSERT INTO `shop_attr` (`id`, `title`, `type`, `unit`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Mariano Kub', 'Marion Dibbert', 'Ut.', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL),
(2, 'Mr. Bart Wisoky PhD', 'Dr. Caleb Schneider', 'Et.', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL),
(3, 'Eloisa Kreiger MD', 'Ansley Will', 'Iste.', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL),
(4, 'Howard Lockman', 'Sigmund Mueller', 'Eum.', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL),
(5, 'Dr. Tracey Rice', 'Claudine Lehner', 'Vel.', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL),
(6, 'Dr. Wallace Conroy II', 'Ms. Alison Denesik DDS', 'Fuga.', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL),
(7, 'Dr. Claire Gerlach III', 'Ms. Destinee Wisoky', 'Quia.', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL),
(8, 'Prof. Garrick Casper I', 'Dixie Huels II', 'Id.', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_attr_val`
--

CREATE TABLE `shop_attr_val` (
  `id` int(10) UNSIGNED NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `shop_attr_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `shop_product_id` int(10) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `shop_attr_val`
--

INSERT INTO `shop_attr_val` (`id`, `value`, `created_at`, `updated_at`, `deleted_at`, `shop_attr_id`, `shop_product_id`) VALUES
(1, 'Clemens Wiegand', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 1, 1),
(2, 'Maverick Muller MD', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 1, 1),
(3, 'Dr. Braulio Bode II', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 1, 1),
(4, 'Dr. Earnestine Wilkinson', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 6, 39),
(5, 'Dr. Rossie Simonis', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 6, 39),
(6, 'Douglas Pfeffer V', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 6, 39),
(7, 'Lottie Champlin', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 6, 39),
(8, 'Jaquan Bartoletti', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 6, 39),
(9, 'Irwin Howell', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 7, 40),
(10, 'Wilfredo Gusikowski', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 7, 40),
(11, 'Mrs. Susie Volkman', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 7, 40),
(12, 'Rhea O\'Hara II', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 7, 40),
(13, 'Emilia Gorczany IV', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 7, 40),
(14, 'Dr. Cale Grant', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 8, 41),
(15, 'Ms. Myah Roberts', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 8, 41),
(16, 'Johnathan Turcotte IV', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 8, 41),
(17, 'Prof. Cydney Hodkiewicz', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 8, 41),
(18, 'Arlene D\'Amore MD', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 8, 41);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_category`
--

CREATE TABLE `shop_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `cover` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `urlhash` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_publication_status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `shop_category`
--

INSERT INTO `shop_category` (`id`, `title`, `description`, `cover`, `icon`, `urlhash`, `category_publication_status`, `created_at`, `updated_at`, `deleted_at`, `parent_id`) VALUES
(1, 'Carmel Leuschke Sr.', 'Queenie Gutmann', 'Hilda Kutch', 'Leonie Connelly', 'http://www.daugherty.com/', 0, '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, NULL),
(2, 'Hannah Larson', 'Kavon Rowe DVM', 'Jennings Flatley PhD', 'Mrs. Lura Zemlak', 'http://www.hamillcartwright.org/qui-nihil-sint-sunt-consequatur', 0, '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, NULL),
(3, 'Hettie Steuber', 'Prof. Therese Waters', 'Rose Dach', 'Dr. Deontae Kilback', 'http://www.kirlinhermann.net/placeat-quia-nobis-tempora-qui-alias-perspiciatis', 0, '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, NULL),
(4, 'Casper Rippin', 'Flavio Schuster DVM', 'Charlie Feeney', 'Lukas Denesik', 'http://okeefeschiller.com/inventore-harum-nihil-quia-et', 0, '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 1),
(5, 'Miss Ora Ortiz', 'Dr. Kiel Sipes', 'Carlie Kunde', 'Althea Dare IV', 'https://www.halvorsonosinski.org/aut-ullam-sed-ut-qui-qui-sunt-harum', 0, '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 1),
(6, 'Margarita Wehner', 'Dr. Louisa Wunsch', 'Alexander Bradtke', 'Bertram Kub', 'http://beermoen.com/veritatis-at-quo-molestias-harum-iusto-delectus-quia', 0, '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 1),
(7, 'Mr. Blaise Koepp Jr.', 'Marta Cruickshank', 'Koby Howell Jr.', 'Jana Berge', 'https://sporer.com/voluptatem-voluptatem-laborum-in-voluptas-esse-incidunt-molestiae.html', 0, '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 1),
(8, 'Juliet Purdy', 'Demarco Morissette', 'Guadalupe Bahringer', 'Adalberto Beatty', 'http://www.ankunding.net/id-laboriosam-reprehenderit-eaque-voluptatibus-perferendis-qui-eaque', 0, '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 1),
(9, 'Concepcion Barrows', 'Jamaal Kub', 'Dr. Mikayla Hermann', 'Seamus Greenholt IV', 'https://schoen.com/in-voluptates-qui-nam-qui-sunt-recusandae-inventore-reiciendis.html', 0, '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 2),
(10, 'Lura Marks', 'Gerald Beier', 'Richmond Stiedemann MD', 'Prof. Paxton Bode', 'http://flatley.com/eligendi-fuga-omnis-voluptatem-recusandae-odio', 0, '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 2),
(11, 'Prof. Donato Murray', 'Sunny Kunde III', 'Jerome Fisher', 'Adrain Stroman', 'https://www.jaskolskibogisich.biz/et-voluptatem-sint-quo-ipsa-rerum-et-placeat', 0, '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 2),
(12, 'Hailey Gerlach', 'Miss Ayla Herzog II', 'Christophe Ziemann', 'Jamarcus Kuhlman', 'https://www.blandamurazik.com/sed-sunt-rerum-odio-et-aut-earum', 0, '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 2),
(13, 'Geovanny Reynolds', 'Miss Elvera Crooks', 'Rosanna Emard', 'Keon Hudson', 'http://www.oconnellcrooks.biz/velit-voluptatem-qui-sit-fuga-est.html', 0, '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 2),
(14, 'Ewald Lebsack', 'Prof. Theodora Hackett IV', 'Dr. Brandyn Crooks II', 'Marlen Keeling', 'http://zulaufweimann.com/voluptatem-vel-est-sunt-alias-dolores-quos', 0, '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 3),
(15, 'Mr. Talon Johnson I', 'Maybell Konopelski', 'Dr. Shania Schroeder', 'Ms. Courtney Runte Sr.', 'http://steuberlind.info/', 0, '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 3),
(16, 'Elyssa Ondricka', 'Dr. Jarvis Weissnat III', 'Chet Gottlieb', 'Lora Greenholt', 'http://cassin.org/ducimus-optio-iste-labore-possimus', 0, '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 3),
(17, 'Darryl Waelchi', 'Mittie Gottlieb', 'Alf Goldner', 'Dr. Malcolm Davis III', 'http://lind.com/eos-dolore-et-et-magni.html', 0, '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 3),
(18, 'Travis Jakubowski PhD', 'Alycia Hegmann', 'Laney Howell', 'Major Spinka', 'http://www.okeefe.net/excepturi-eos-omnis-eius-ipsum.html', 0, '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 3),
(19, 'Miss Cheyanne Klein', 'Shane Goldner', 'Miss Nichole Ankunding I', 'Fannie Lindgren', 'http://www.stark.com/asperiores-dicta-iste-eveniet-iure-molestiae', 0, '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, NULL),
(20, 'Wilma Zieme', 'Glen Stoltenberg', 'Brycen Conn', 'Hank Breitenberg', 'https://www.brown.com/minus-excepturi-repudiandae-aut-et-pariatur', 0, '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, NULL),
(21, 'Miss Elisha Schneider MD', 'Lance Lynch', 'Ms. Francisca Moore IV', 'Giovani Barrows', 'http://mayert.com/enim-itaque-ipsum-ea-dolores', 0, '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_category_attr`
--

CREATE TABLE `shop_category_attr` (
  `id` int(10) UNSIGNED NOT NULL,
  `weigth` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `shop_category_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `shop_attr_id` int(10) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_clients`
--

CREATE TABLE `shop_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `shop_clients`
--

INSERT INTO `shop_clients` (`id`, `name`, `email`, `phone`, `created_at`, `updated_at`) VALUES
(1, 'Dr. Brittany Schneider', 'powlowski.norbert@yahoo.com', '(409)279-2527x1210', '2018-10-16 05:55:55', '2018-10-16 05:55:55'),
(2, 'Reece Kihn', 'marquardt.benjamin@yahoo.com', '581.223.9527x01519', '2018-10-16 05:55:55', '2018-10-16 05:55:55'),
(3, 'Melisa Schroeder V', 'bwest@braunmertz.org', '+67(6)2952477230', '2018-10-16 05:55:55', '2018-10-16 05:55:55'),
(4, 'Nathanial Harvey', 'samanta.raynor@collier.com', '1-585-199-7768x8380', '2018-10-16 05:55:55', '2018-10-16 05:55:55'),
(5, 'Juanita Kovacek', 'dane95@hotmail.com', '01354188259', '2018-10-16 05:55:55', '2018-10-16 05:55:55'),
(6, 'Aiyana Hand', 'jacobs.lisandro@bogisich.com', '394-244-5232x68717', '2018-10-16 05:55:55', '2018-10-16 05:55:55'),
(7, 'Graham Christiansen', 'davis.aliya@hotmail.com', '08564436958', '2018-10-16 05:55:55', '2018-10-16 05:55:55'),
(8, 'Cyril Braun', 'myrtice47@yahoo.com', '04619467828', '2018-10-16 05:55:55', '2018-10-16 05:55:55'),
(9, 'Ernestina Schowalter', 'nader.jay@yahoo.com', '+27(2)4675172235', '2018-10-16 05:55:55', '2018-10-16 05:55:55'),
(10, 'Lonzo Keebler', 'ryan14@schmeler.com', '199.776.0860', '2018-10-16 05:55:55', '2018-10-16 05:55:55'),
(11, 'Mr. Issac Johnston', 'xwalker@hotmail.com', '+84(6)1117832864', '2018-10-16 05:55:55', '2018-10-16 05:55:55'),
(12, 'Dana Weimann', 'ivory62@gleichnerrolfson.com', '1-180-348-2819x42319', '2018-10-16 05:55:55', '2018-10-16 05:55:55'),
(13, 'Prof. Elfrieda Erdman Sr.', 'vwilliamson@wolfvon.net', '(058)299-2789', '2018-10-16 05:55:55', '2018-10-16 05:55:55');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_comments`
--

CREATE TABLE `shop_comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `msg` text COLLATE utf8mb4_unicode_ci,
  `approve` enum('true','false') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'false',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `shop_product_id` int(10) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `shop_comments`
--

INSERT INTO `shop_comments` (`id`, `name`, `email`, `msg`, `approve`, `created_at`, `updated_at`, `deleted_at`, `shop_product_id`) VALUES
(1, 'Blake Donnelly', 'kiara.d\'amore@weimann.com', 'Cheshire Cat sitting on the top of her or of.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 1),
(2, 'Eleanore Carter DVM', 'stanton42@ullrich.com', 'I THINK,\' said Alice. \'And be quick about it,\'.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 1),
(3, 'Dudley Parisian', 'luella81@gmail.com', 'ME.\' \'You!\' said the Cat. \'I said pig,\' replied.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 1),
(4, 'Edmond Gerhold', 'trudie04@schinnerleuschke.org', 'I will prosecute YOU.--Come, I\'ll take no.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 1),
(5, 'Lyla Gottlieb', 'catalina18@hotmail.com', 'Duchess asked, with another dig of her head to.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 1),
(6, 'Dr. Anderson Kshlerin III', 'fay.devon@little.com', 'Footman went on in a low voice, to the table,.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 1),
(7, 'Prof. Anthony Schulist', 'denesik.adrienne@yahoo.com', 'She had not gone much farther before she gave a.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 1),
(8, 'Cecile Pollich', 'talon63@hotmail.com', 'However, \'jury-men\' would have appeared to them.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 1),
(9, 'Dr. Larry Funk DVM', 'udare@gmail.com', 'AND WASHING--extra.\"\' \'You couldn\'t have done.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 1),
(10, 'Prof. Justina Greenholt DDS', 'arath@kinghammes.net', 'The hedgehog was engaged in a deep voice, \'What.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 1),
(11, 'Casimer Mraz I', 'stacey.mckenzie@gmail.com', 'Caterpillar. Here was another puzzling question;.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 19),
(12, 'Prof. Wilhelm Schmitt', 'roman.morar@dickenskoepp.com', 'Queen. \'Their heads are gone, if it makes rather.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 19),
(13, 'Zachery Rippin', 'little.julianne@hotmail.com', 'Queen till she was now about a whiting before.\'.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 19),
(14, 'Keaton Moore', 'tatum.toy@gmail.com', 'I see\"!\' \'You might just as she could. \'The.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 19),
(15, 'Jeremy Howe', 'hdeckow@gmail.com', 'Alice thought to herself as she fell very.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 19),
(16, 'Dr. Juvenal Stokes V', 'noelia.monahan@gmail.com', 'I suppose, by being drowned in my life!\' She had.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 19),
(17, 'Monserrate Wilkinson', 'jhodkiewicz@gloverparker.info', 'Alice. \'But you\'re so easily offended!\' \'You\'ll.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 19),
(18, 'Prof. Declan Rohan', 'rodriguez.laury@hayes.com', 'Gryphon repeated impatiently: \'it begins \"I.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 19),
(19, 'Mr. Arnulfo Hahn', 'volson@hotmail.com', 'Alice, a good character, But said I could shut.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 19),
(20, 'Ms. Kassandra Moore', 'wgibson@gmail.com', 'THAT direction,\' the Cat in a low, hurried tone..', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 19),
(21, 'Susanna Hayes', 'concepcion.hegmann@gmail.com', 'I suppose?\' \'Yes,\' said Alice, seriously, \'I\'ll.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 20),
(22, 'Wyatt Kihn', 'hansen.zakary@hammes.com', 'Lobster Quadrille, that she began very.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 20),
(23, 'Dr. Carolyn Kuhlman V', 'hilda82@yahoo.com', 'MYSELF, I\'m afraid, but you might do something.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 20),
(24, 'Dustin Cormier IV', 'titus.hilll@erdman.org', 'Dinah, and saying to her great disappointment it.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 20),
(25, 'Mr. Guido Gulgowski Sr.', 'barry.hyatt@batz.net', 'There was a queer-shaped little creature, and.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 20),
(26, 'Dr. Franco Terry Sr.', 'antonia.wyman@dare.com', 'Knave of Hearts, he stole those tarts, And took.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 20),
(27, 'Anabelle Swift', 'theo07@abernathy.biz', 'I can\'t quite follow it as well wait, as she.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 20),
(28, 'Enid Beier DVM', 'wunsch.alison@schaden.com', 'Alice, \'it\'ll never do to hold it. As soon as.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 20),
(29, 'Prof. Aliya Parisian', 'mlang@lynchprosacco.com', 'Dinah, and saying to her chin in salt water. Her.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 20),
(30, 'Jacey Reichel', 'pturner@hotmail.com', 'Alice, \'it\'ll never do to ask: perhaps I shall.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 20),
(31, 'Jazlyn Kohler', 'bfahey@thompsonjacobi.com', 'The Cat only grinned a little feeble, squeaking.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 21),
(32, 'Ezra Fritsch IV', 'egoyette@gmail.com', 'I\'ll have you got in your pocket?\' he went on in.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 21),
(33, 'Vernon Rutherford', 'ckerluke@gmail.com', 'This did not much like keeping so close to her,.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 21),
(34, 'Michael Bogan', 'wcrist@hotmail.com', 'PLEASE mind what you\'re talking about,\' said.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 21),
(35, 'Serena Barton I', 'whilpert@dickikrajcik.info', 'THEY GAVE HIM TWO--\" why, that must be collected.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 21),
(36, 'Corrine Balistreri', 'ulehner@gmail.com', 'She went in search of her ever getting out of.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 21),
(37, 'Jordon Klein I', 'lemuel.krajcik@yahoo.com', 'Mock Turtle interrupted, \'if you don\'t know.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 21),
(38, 'Lester Kirlin', 'khoeger@schmidt.net', 'Let me see: I\'ll give them a new kind of.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 21),
(39, 'Jean Hills', 'autumn.aufderhar@yahoo.com', 'Queen till she had someone to listen to me! I\'LL.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 21),
(40, 'Gladyce Walker IV', 'royce.moen@gmail.com', 'The rabbit-hole went straight on like a frog;.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 21),
(41, 'Hailee Maggio', 'abby68@gerlachstark.biz', 'But her sister sat still and said \'What else.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 22),
(42, 'Melyna Tromp', 'tillman.anastacio@hotmail.com', 'Queen to-day?\' \'I should have liked teaching it.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 22),
(43, 'Alaina Lesch', 'schiller.johnson@upton.com', 'The Hatter opened his eyes very wide on hearing.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 22),
(44, 'Paula McKenzie', 'ayla.schimmel@hotmail.com', 'I wonder?\' And here poor Alice began to say.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 22),
(45, 'Prof. Mellie Cassin', 'novella88@willms.com', 'SOMEBODY ought to be said. At last the Dodo.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 22),
(46, 'Mrs. Ana Cummerata', 'ihegmann@bins.biz', 'ME.\' \'You!\' said the Mock Turtle, \'they--you\'ve.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 22),
(47, 'Georgiana Powlowski', 'ottis.nienow@hotmail.com', 'I shall have to whisper a hint to Time, and.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 22),
(48, 'Amelia Dietrich', 'cloyd.lang@yahoo.com', 'Alice whispered to the waving of the wood--(she.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 22),
(49, 'Savanah Bruen DDS', 'cbins@gmail.com', 'YOU like cats if you cut your finger VERY deeply.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 22),
(50, 'Cassie Kessler II', 'chandler.runolfsson@kihn.org', 'But at any rate I\'ll never go THERE again!\' said.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 22),
(51, 'Dr. Zoila Mraz IV', 'qlittel@ritchiegleason.com', 'Alice ventured to ask. \'Suppose we change the.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 23),
(52, 'Prof. Javonte Zulauf IV', 'thurman34@bosco.com', 'Alice. \'Of course you know I\'m mad?\' said Alice..', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 23),
(53, 'Walter Tremblay', 'mayra13@gmail.com', 'I ever heard!\' \'Yes, I think it so yet,\' said.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 23),
(54, 'Lorenz Hirthe', 'rubye.o\'keefe@hotmail.com', 'Which shall sing?\' \'Oh, YOU sing,\' said the.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 23),
(55, 'Mona Beier DDS', 'elwyn73@carter.com', 'Alice. \'Stand up and ran till she got back to.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 23),
(56, 'Sandy Funk', 'fnader@bernhard.com', 'Duck. \'Found IT,\' the Mouse was bristling all.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 23),
(57, 'Mr. Jessy Stehr II', 'prosacco.emil@schulist.info', 'YET,\' she said this, she came upon a Gryphon,.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 23),
(58, 'Marcus Harvey', 'alana.huel@considine.info', 'I shall think nothing of the jurymen. \'No,.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 23),
(59, 'Pearline Bashirian', 'korey.rempel@hudson.com', 'Alice dodged behind a great hurry, muttering to.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 23),
(60, 'Chelsea Hills', 'stokes.opal@hotmail.com', 'I almost think I can say.\' This was quite out of.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 23),
(61, 'Aileen Hessel Jr.', 'zschmitt@heathcote.biz', 'Come on!\' So they got thrown out to be no sort.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 24),
(62, 'Prof. Fermin Weissnat III', 'vcormier@westhoppe.com', 'Elsie, Lacie, and Tillie; and they walked off.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 24),
(63, 'Quinn Schuppe III', 'jaren36@yahoo.com', 'Gryphon. \'We can do no more, whatever happens..', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 24),
(64, 'Prof. Jessika Halvorson', 'eloisa.beier@hotmail.com', 'As for pulling me out of the trial.\' \'Stupid.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 24),
(65, 'Willie Kovacek', 'abernathy.clyde@yahoo.com', 'May it won\'t be raving mad after all! I almost.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 24),
(66, 'Elissa Dooley', 'maxwell87@murazik.com', 'Alice: \'three inches is such a very difficult.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 24),
(67, 'Stevie Reilly', 'dooley.orlo@gmail.com', 'Gryphon: \'I went to the little creature down,.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 24),
(68, 'Idell Huels', 'antonetta07@hotmail.com', 'Queen, turning purple. \'I won\'t!\' said Alice..', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 24),
(69, 'Felipa Zemlak III', 'oroob@hotmail.com', 'Hatter, and, just as usual. \'Come, there\'s no.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 24),
(70, 'Carolina Reynolds DVM', 'lakin.olin@effertz.com', 'These were the cook, to see a little hot tea.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 24),
(71, 'Addison Gottlieb', 'fkerluke@lehnernienow.com', 'THAT direction,\' waving the other side of the.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 25),
(72, 'Mariane Krajcik', 'mohamed.dare@yahoo.com', 'Dormouse again, so violently, that she had.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 25),
(73, 'Candace Lynch', 'ryan.irwin@yahoo.com', 'Hatter: \'as the things I used to it!\' pleaded.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 25),
(74, 'Jarrod Borer IV', 'cartwright.rickie@yahoo.com', 'March Hare. \'Yes, please do!\' but the great.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 25),
(75, 'Ms. Hertha Hintz', 'ana41@walker.com', 'She took down a very deep well. Either the well.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 25),
(76, 'Ms. Melyssa Bins', 'constance72@gmail.com', 'Some of the lefthand bit. * * * * * * * * * * *.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 25),
(77, 'Roger Connelly', 'fzulauf@yahoo.com', 'Turtle.\' These words were followed by a very.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 25),
(78, 'Ms. Caterina Jones DDS', 'mjerde@gmail.com', 'Poor Alice! It was the first position in which.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 25),
(79, 'Dee Dicki', 'maurine.kuphal@donnellybuckridge.net', 'She hastily put down the chimney!\' \'Oh! So.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 25),
(80, 'Mr. Adelbert Wisozk DVM', 'philip46@yahoo.com', 'Alice remarked. \'Right, as usual,\' said the.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 25),
(81, 'Dr. Althea Daniel', 'ncronin@hilll.com', 'She waited for a long hookah, and taking not the.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 26),
(82, 'Dr. Shayna Hickle Sr.', 'marvin.johnson@ondrickalangosh.org', 'Run home this moment, I tell you, you coward!\'.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 26),
(83, 'Coty Adams', 'maribel.conroy@hartmann.biz', 'Alice. One of the bread-and-butter. Just at this.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 26),
(84, 'Miss Kelsi Huels', 'veum.malvina@yahoo.com', 'Gryphon. \'Of course,\' the Gryphon as if it had a.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 26),
(85, 'Dr. Cielo Connelly', 'jewel96@mcdermottvolkman.com', 'I know is, it would like the look of it had.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 26),
(86, 'Mrs. Coralie Stanton DDS', 'lambert.jacobs@gmail.com', 'I\'ll tell him--it was for bringing the cook was.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 26),
(87, 'Daron Beier', 'aboyle@yahoo.com', 'I never was so small as this is May it won\'t be.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 26),
(88, 'Berta Robel', 'hulda.littel@hotmail.com', 'Bill!\' then the Rabbit\'s voice along--\'Catch.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 26),
(89, 'Juston Effertz', 'abernathy.macy@gmail.com', 'VERY good opportunity for showing off a little.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 26),
(90, 'Miss Kitty McCullough V', 'beulah.smitham@gmail.com', 'Hatter. \'You MUST remember,\' remarked the King,.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 26),
(91, 'Yazmin DuBuque', 'angel35@yahoo.com', 'There\'s no pleasing them!\' Alice was not a bit.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 27),
(92, 'Dr. Olen Windler Sr.', 'aparisian@yahoo.com', 'At last the Caterpillar called after her. \'I\'ve.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 27),
(93, 'Celestine Osinski', 'meredith.rau@stoltenberg.com', 'Caterpillar decidedly, and the King exclaimed,.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 27),
(94, 'Karley Schumm', 'neal25@hotmail.com', 'Who for such dainties would not allow without.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 27),
(95, 'Fleta Crist', 'brad.hoeger@hotmail.com', 'Caterpillar. Here was another puzzling question;.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 27),
(96, 'Montana Bednar', 'raynor.roscoe@yahoo.com', 'Next came an angry tone, \'Why, Mary Ann, and be.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 27),
(97, 'Alessandro Cassin', 'gkoss@hotmail.com', 'I can find them.\' As she said to Alice; and.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 27),
(98, 'Gennaro Auer', 'ellis.tillman@cole.com', 'The master was an old crab, HE was.\' \'I never.', 'false', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 27),
(99, 'Mabelle Moore', 'miguel.mcclure@gmail.com', 'Alice as he wore his crown over the edge of her.', 'true', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 27),
(100, 'Pinkie Heller', 'ignatius.ward@okunevarenner.com', 'I look like it?\' he said, turning to the shore,.', 'false', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 27),
(101, 'Belle Schaden', 'schaden.casimer@yahoo.com', 'ME\' beautifully printed on it (as she had to.', 'true', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 28),
(102, 'Miss Ludie Douglas', 'kristofer.fritsch@casper.biz', 'The first question of course was, how to speak.', 'true', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 28),
(103, 'Antonietta Kutch', 'ylueilwitz@hotmail.com', 'King. \'Nearly two miles high,\' added the.', 'true', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 28),
(104, 'Eudora Trantow Jr.', 'skunde@deckow.org', 'I shall see it written up somewhere.\' Down,.', 'false', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 28),
(105, 'Dr. Jayne Towne IV', 'haley.tomas@yahoo.com', 'Gryphon as if nothing had happened. \'How am I.', 'true', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 28),
(106, 'Rene Streich', 'gutmann.fredy@hudsonernser.biz', 'Queen\'s voice in the kitchen. \'When I\'M a.', 'true', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 28),
(107, 'Mrs. Meta Beer', 'steuber.casandra@hotmail.com', 'I should have croqueted the Queen\'s shrill cries.', 'false', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 28),
(108, 'Alycia Torphy', 'hannah.mcclure@conroy.biz', 'The long grass rustled at her feet as the jury.', 'false', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 28),
(109, 'Mr. Dereck Pfeffer DVM', 'streich.jovany@koss.net', 'Gryphon is, look at them--\'I wish they\'d get the.', 'false', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 28),
(110, 'Manuel Conroy', 'pturner@connellystanton.com', 'Then the Queen to play croquet.\' The.', 'true', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 28);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_gallaries`
--

CREATE TABLE `shop_gallaries` (
  `id` int(10) UNSIGNED NOT NULL,
  `filename` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `shop_gallaries`
--

INSERT INTO `shop_gallaries` (`id`, `filename`, `created_at`, `updated_at`) VALUES
(1, 'http://lorempixel.com/300/400/', '2018-10-16 05:55:54', '2018-10-16 05:55:54'),
(2, 'http://lorempixel.com/300/400/', '2018-10-16 05:55:54', '2018-10-16 05:55:54'),
(3, 'http://lorempixel.com/300/400/', '2018-10-16 05:55:54', '2018-10-16 05:55:54'),
(4, 'http://lorempixel.com/300/400/', '2018-10-16 05:55:54', '2018-10-16 05:55:54'),
(5, 'http://lorempixel.com/300/400/', '2018-10-16 05:55:54', '2018-10-16 05:55:54'),
(6, 'http://lorempixel.com/300/400/', '2018-10-16 05:55:54', '2018-10-16 05:55:54'),
(7, 'http://lorempixel.com/300/400/', '2018-10-16 05:55:55', '2018-10-16 05:55:55'),
(8, 'http://lorempixel.com/300/400/', '2018-10-16 05:55:55', '2018-10-16 05:55:55'),
(9, 'http://lorempixel.com/300/400/', '2018-10-16 05:55:55', '2018-10-16 05:55:55'),
(10, 'http://lorempixel.com/300/400/', '2018-10-16 05:55:55', '2018-10-16 05:55:55'),
(11, 'http://lorempixel.com/300/400/', '2018-10-16 05:55:55', '2018-10-16 05:55:55'),
(12, 'http://lorempixel.com/300/400/', '2018-10-16 05:55:55', '2018-10-16 05:55:55'),
(13, 'http://lorempixel.com/300/400/', '2018-10-16 05:55:55', '2018-10-16 05:55:55'),
(14, 'http://lorempixel.com/300/400/', '2018-10-16 05:55:55', '2018-10-16 05:55:55'),
(15, 'http://lorempixel.com/300/400/', '2018-10-16 05:55:55', '2018-10-16 05:55:55'),
(16, 'http://lorempixel.com/300/400/', '2018-10-16 05:55:55', '2018-10-16 05:55:55');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_info`
--

CREATE TABLE `shop_info` (
  `id` int(10) UNSIGNED NOT NULL,
  `text` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `shop_info`
--

INSERT INTO `shop_info` (`id`, `text`, `created_at`, `updated_at`) VALUES
(1, 'I\'m talking!\' Just.', '2018-10-16 05:55:55', '2018-10-16 05:55:55'),
(2, 'Go on!\' \'I\'m a.', '2018-10-16 05:55:55', '2018-10-16 05:55:55'),
(3, 'Queen said to the.', '2018-10-16 05:55:55', '2018-10-16 05:55:55'),
(4, 'Ugh, Serpent!\'.', '2018-10-16 05:55:55', '2018-10-16 05:55:55'),
(5, 'Queen. \'You make.', '2018-10-16 05:55:55', '2018-10-16 05:55:55'),
(6, 'Mock Turtle, \'but.', '2018-10-16 05:55:55', '2018-10-16 05:55:55'),
(7, 'This of course, I.', '2018-10-16 05:55:55', '2018-10-16 05:55:55'),
(8, 'I\'LL soon make you.', '2018-10-16 05:55:55', '2018-10-16 05:55:55'),
(9, 'As soon as look at.', '2018-10-16 05:55:55', '2018-10-16 05:55:55'),
(10, 'Come on!\' So they.', '2018-10-16 05:55:55', '2018-10-16 05:55:55');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_jobs`
--

CREATE TABLE `shop_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `shop_jobs`
--

INSERT INTO `shop_jobs` (`id`, `queue`, `payload`, `attempts`, `reserved`, `reserved_at`, `available_at`, `created_at`, `updated_at`) VALUES
(1, '7', 'Now I.', 2, 7, NULL, 6, '2018-10-16 05:55:55', '2018-10-16 05:55:55'),
(2, '1', 'English.', 5, 5, NULL, 0, '2018-10-16 05:55:55', '2018-10-16 05:55:55'),
(3, '0', 'Alice,.', 2, 7, NULL, 4, '2018-10-16 05:55:55', '2018-10-16 05:55:55'),
(4, '3', 'Alice,.', 1, 3, NULL, 3, '2018-10-16 05:55:55', '2018-10-16 05:55:55'),
(5, '8', 'I hadn\'t.', 9, 7, NULL, 0, '2018-10-16 05:55:55', '2018-10-16 05:55:55');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_npcity`
--

CREATE TABLE `shop_npcity` (
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ref` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `shop_npcity`
--

INSERT INTO `shop_npcity` (`name`, `ref`) VALUES
('Katherineshire', 'http://nitzschewisoky.com/adipisci-voluptatem-reprehenderit-aliquam-harum'),
('Hermistonport', 'https://www.volkman.net/iusto-nobis-rerum-iure-nisi'),
('West Madysonborough', 'http://www.mrazboyer.com/nesciunt-at-neque-exercitationem-quod-sit-excepturi'),
('North Orphaport', 'https://wunschgorczany.org/autem-temporibus-est-voluptatem-a-culpa-illum.html'),
('Schadentown', 'http://botsford.com/sint-exercitationem-est-aliquid-vel-non-qui-ab.html'),
('Kelliside', 'https://www.graham.com/est-velit-voluptas-est-ratione-aut-quisquam-delectus'),
('Shannonshire', 'http://www.koelpingleason.com/nulla-rem-et-ex-qui-vitae-eaque-voluptas.html'),
('North Noemystad', 'https://www.klein.com/ea-labore-temporibus-aliquid-dolorem'),
('Nathanielstad', 'http://heller.com/provident-eligendi-quaerat-est-enim-laborum-nihil'),
('Port Jazminview', 'http://mcclure.com/ipsa-quidem-consequatur-ut-deserunt');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_npunit`
--

CREATE TABLE `shop_npunit` (
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ref` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `shop_npunit`
--

INSERT INTO `shop_npunit` (`name`, `ref`) VALUES
('Rosendo Blick', 'http://www.schinner.com/in-distinctio-voluptatem-numquam'),
('Krystina Hintz', 'http://klockorosenbaum.com/'),
('Mr. Tyshawn Schumm MD', 'https://www.okeefe.info/animi-voluptatum-odio-sunt-sint'),
('Keagan Reichel', 'https://www.nitzsche.biz/dolor-velit-aut-fuga-et-pariatur-sunt-tempora'),
('Zoe Hayes', 'http://www.stroman.com/'),
('Prof. Conner Skiles', 'http://kuhn.com/voluptates-facilis-nesciunt-eaque-sunt-ullam.html'),
('Rogelio Roob', 'http://www.mcglynn.com/non-perspiciatis-praesentium-culpa-assumenda-rerum-fugiat'),
('Kailyn Bruen', 'http://www.kertzmann.com/cum-quidem-fugiat-aut-assumenda-ut-voluptates-aut'),
('Randi Crona', 'http://www.predovic.com/a-quibusdam-ut-rerum-eos-praesentium'),
('Enos Gaylord', 'http://www.raynorbauch.com/');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_orders`
--

CREATE TABLE `shop_orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `delivery_city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delivery_adr` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delivery_np` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delivery_type` enum('adr','np') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'np',
  `pay_type` enum('null','privat24','privat_terminal','liqpay') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'privat24',
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ttn` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment` longtext COLLATE utf8mb4_unicode_ci,
  `status` enum('new','paid','sent') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'new',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `shop_client_id` int(10) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `shop_orders`
--

INSERT INTO `shop_orders` (`id`, `delivery_city`, `delivery_adr`, `delivery_np`, `delivery_type`, `pay_type`, `code`, `ttn`, `comment`, `status`, `created_at`, `updated_at`, `shop_client_id`) VALUES
(1, 'Lake Robyn', NULL, NULL, 'np', 'privat_terminal', 'IN', NULL, NULL, 'new', '2018-10-16 05:55:55', '2018-10-16 05:55:55', 1),
(2, 'Lake Derektown', NULL, NULL, 'np', 'privat24', 'MX', NULL, NULL, 'paid', '2018-10-16 05:55:55', '2018-10-16 05:55:55', 1),
(3, 'Savanahfurt', NULL, NULL, 'adr', 'liqpay', 'FR', NULL, NULL, 'new', '2018-10-16 05:55:55', '2018-10-16 05:55:55', 1),
(4, 'Wintheisermouth', NULL, NULL, 'np', 'liqpay', 'FR', NULL, NULL, 'sent', '2018-10-16 05:55:55', '2018-10-16 05:55:55', 11),
(5, 'North Taylortown', NULL, NULL, 'adr', 'privat_terminal', 'US', NULL, NULL, 'paid', '2018-10-16 05:55:55', '2018-10-16 05:55:55', 11),
(6, 'West Karlville', NULL, NULL, 'adr', 'null', 'MX', NULL, NULL, 'sent', '2018-10-16 05:55:55', '2018-10-16 05:55:55', 11),
(7, 'Lake Krystal', NULL, NULL, 'np', 'privat24', 'GB', NULL, NULL, 'new', '2018-10-16 05:55:55', '2018-10-16 05:55:55', 11),
(8, 'North Brandt', NULL, NULL, 'np', 'liqpay', 'PT', NULL, NULL, 'new', '2018-10-16 05:55:55', '2018-10-16 05:55:55', 11),
(9, 'Loyalville', NULL, NULL, 'adr', 'privat24', 'IN', NULL, NULL, 'new', '2018-10-16 05:55:55', '2018-10-16 05:55:55', 12),
(10, 'East Jamirfort', NULL, NULL, 'adr', 'privat24', 'RU', NULL, NULL, 'new', '2018-10-16 05:55:55', '2018-10-16 05:55:55', 12),
(11, 'Bartellfurt', NULL, NULL, 'adr', 'privat24', 'IT', NULL, NULL, 'paid', '2018-10-16 05:55:55', '2018-10-16 05:55:55', 12),
(12, 'South Leolastad', NULL, NULL, 'adr', 'privat_terminal', 'DE', NULL, NULL, 'sent', '2018-10-16 05:55:55', '2018-10-16 05:55:55', 12),
(13, 'McGlynnshire', NULL, NULL, 'adr', 'privat_terminal', 'CN', NULL, NULL, 'paid', '2018-10-16 05:55:55', '2018-10-16 05:55:55', 12),
(14, 'Turnerhaven', NULL, NULL, 'np', 'liqpay', 'US', NULL, NULL, 'sent', '2018-10-16 05:55:55', '2018-10-16 05:55:55', 13),
(15, 'Borershire', NULL, NULL, 'np', 'privat24', 'MX', NULL, NULL, 'paid', '2018-10-16 05:55:55', '2018-10-16 05:55:55', 13),
(16, 'Lowellmouth', NULL, NULL, 'adr', 'liqpay', 'ES', NULL, NULL, 'paid', '2018-10-16 05:55:55', '2018-10-16 05:55:55', 13),
(17, 'Lionelhaven', NULL, NULL, 'np', 'privat_terminal', 'IN', NULL, NULL, 'new', '2018-10-16 05:55:55', '2018-10-16 05:55:55', 13),
(18, 'Howellburgh', NULL, NULL, 'np', 'liqpay', 'CA', NULL, NULL, 'new', '2018-10-16 05:55:55', '2018-10-16 05:55:55', 13),
(19, 'West Perryfort', NULL, NULL, 'adr', 'liqpay', 'RU', NULL, NULL, 'new', '2018-10-16 05:55:55', '2018-10-16 05:55:55', 1),
(20, 'Port Shayna', NULL, NULL, 'np', 'liqpay', 'IN', NULL, NULL, 'paid', '2018-10-16 05:55:55', '2018-10-16 05:55:55', 1),
(21, 'North Janick', NULL, NULL, 'adr', 'null', 'RU', NULL, NULL, 'sent', '2018-10-16 05:55:55', '2018-10-16 05:55:55', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_order_add`
--

CREATE TABLE `shop_order_add` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `shop_order_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `shop_additional_id` int(10) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_order_files`
--

CREATE TABLE `shop_order_files` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hash` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mime` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extension` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('tmp','success') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'tmp',
  `image` enum('true','false') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'false',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `shop_order_id` int(10) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `shop_order_files`
--

INSERT INTO `shop_order_files` (`id`, `name`, `hash`, `mime`, `extension`, `status`, `image`, `created_at`, `updated_at`, `shop_order_id`) VALUES
(1, 'Sadyeville', 'f5f30962418e8458c3a10fc825c85bd7546fb84abd4520ab4fc59390dd6d826f', 'application/vnd.oasis.opendocument.presentation-template', 'Alice quietly.', 'tmp', 'true', '2018-10-16 05:55:55', '2018-10-16 05:55:55', 1),
(2, 'Retaland', '9e32d877e5781c39371ec106a59e9dd5d1bc9cbcc053afac4100e042694e7b2e', 'application/vnd.visionary', 'Alice knew it was.', 'success', 'true', '2018-10-16 05:55:55', '2018-10-16 05:55:55', 1),
(3, 'North Jodieside', '6113d45799e0f716737e43f59b5135895435323120f632564d65c0a8c98686a3', 'application/vnd.flographit', 'IS that to be.', 'success', 'true', '2018-10-16 05:55:55', '2018-10-16 05:55:55', 1),
(4, 'South Nelson', '22b95e23a78357861977992ee57ca3bf656b86bbe0603a352b0d366413d52691', 'model/x3d+binary', 'I ask! It\'s always.', 'success', 'false', '2018-10-16 05:55:55', '2018-10-16 05:55:55', 19),
(5, 'New Friedrichland', 'ed9cab572672e78990d78230eb2da54c1a4ee9f5a200a4a16eb858c8b176d6d7', 'application/vnd.oasis.opendocument.formula', 'I begin, please.', 'success', 'true', '2018-10-16 05:55:55', '2018-10-16 05:55:55', 19),
(6, 'Nyahfurt', '5b8f56d4f83b4118f28f2767ec8fdf8f2e3e9daf5eb4610add2bdf4ccaf0206d', 'application/vnd.hydrostatix.sof-data', 'Pray, what is the.', 'tmp', 'true', '2018-10-16 05:55:55', '2018-10-16 05:55:55', 19),
(7, 'New Waynemouth', '37e6bbf7c71089a794f1673b50d24166a3015dd9a56e7ae8ba7d356596f2083b', 'application/x-sh', 'Alice said very.', 'success', 'false', '2018-10-16 05:55:55', '2018-10-16 05:55:55', 19),
(8, 'Ullrichland', '866607393e77b055c70db07776270f714a66651037e27fb6efb69e6434c8d891', 'application/pgp-encrypted', 'Alice. \'Exactly.', 'success', 'true', '2018-10-16 05:55:55', '2018-10-16 05:55:55', 19),
(9, 'East Katarina', '5d3f2cdd3d837a0e33a2177195baea86aa6773cf59129248a291b604e2e6a28e', 'video/x-msvideo', 'The Cat seemed to.', 'tmp', 'false', '2018-10-16 05:55:55', '2018-10-16 05:55:55', 20),
(10, 'Weissnatside', '5de706a2e2bd5bf10de68dbf50629db6d32ff04bbd0bf3ebc7d94b9b5179799d', 'application/vnd.olpc-sugar', 'Shakespeare, in.', 'success', 'false', '2018-10-16 05:55:55', '2018-10-16 05:55:55', 20),
(11, 'Lake Liliane', '4d0b8ab8e6d13390f826b25c7a5bd4c22857a910069839bf2df00eddd198c849', 'application/vnd.ms-powerpoint.template.macroenabled.12', 'Alice. \'Reeling.', 'tmp', 'false', '2018-10-16 05:55:55', '2018-10-16 05:55:55', 20),
(12, 'East Quincy', '40a532a9f4bdb7d018b1e8a90fc9c327a134432f3b2aa9200eda37b29939644e', 'image/x-3ds', 'I like\"!\' \'You.', 'success', 'false', '2018-10-16 05:55:55', '2018-10-16 05:55:55', 20),
(13, 'South Bryana', '28d2ad37e6bd4a0a99553772937a4c9ec247ff8aa956bd1cfcdd88eec5206494', 'text/plain', 'The three soldiers.', 'tmp', 'false', '2018-10-16 05:55:55', '2018-10-16 05:55:55', 20),
(14, 'Lake Leilanibury', 'e66257a9785c9ecd7db50bee187c30ccee20eb28581fa905cddd52c19b7dc168', 'application/vnd.unity', 'I\'d only been the.', 'success', 'false', '2018-10-16 05:55:55', '2018-10-16 05:55:55', 21),
(15, 'Lorenzaton', 'a31e5f0ab6fda2e84017b3e9f16d9b7426b67e22d81fe389c96f8bfecba0ef3d', 'application/vnd.dpgraph', 'And he got up this.', 'tmp', 'false', '2018-10-16 05:55:55', '2018-10-16 05:55:55', 21),
(16, 'Port Ashton', '7d329f2538062ae665d232d579036c5cd09e5301d62b7bfd3a5913613630aeb2', 'application/vnd.las.las+xml', 'Gryphon remarked:.', 'success', 'true', '2018-10-16 05:55:55', '2018-10-16 05:55:55', 21),
(17, 'East Maybellemouth', '053a21bce83b0b1ada817c9a43dd42888f4ae63224f6d068aae117b75926af01', 'video/h264', 'Hatter went on,.', 'tmp', 'false', '2018-10-16 05:55:55', '2018-10-16 05:55:55', 21),
(18, 'Arielleport', '071ab8fbe068ff99e2791fd738c0d71ba0e274ffb46a5e029814e06dc9fc0153', 'application/vnd.ms-fontobject', 'As they walked off.', 'success', 'true', '2018-10-16 05:55:55', '2018-10-16 05:55:55', 21);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_order_items`
--

CREATE TABLE `shop_order_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `qty` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `shop_order_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `shop_product_id` int(10) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_products`
--

CREATE TABLE `shop_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `description_full` text COLLATE utf8mb4_unicode_ci,
  `values` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cover` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sku` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` double NOT NULL,
  `price_old` double DEFAULT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isset` enum('true','false') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'true',
  `visible` enum('true','false') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'true',
  `urlhash` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `category_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `shop_gallery_id` int(10) UNSIGNED DEFAULT NULL,
  `shop_gallery_back_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `shop_products`
--

INSERT INTO `shop_products` (`id`, `title`, `name`, `keywords`, `description`, `description_full`, `values`, `cover`, `sku`, `price`, `price_old`, `label`, `isset`, `visible`, `urlhash`, `created_at`, `updated_at`, `deleted_at`, `category_id`, `parent_id`, `shop_gallery_id`, `shop_gallery_back_id`) VALUES
(1, 'Darius Ankunding', 'Dr. Amanda Schmitt', NULL, 'Officiis sapiente soluta vel dignissimos. Ullam et voluptatem esse saepe quisquam laboriosam. Atque expedita ea voluptas nulla repudiandae sunt.\nVero velit et placeat atque deserunt earum. Enim velit modi quaerat quis voluptas perferendis.', NULL, NULL, NULL, NULL, 2, NULL, NULL, 'true', 'false', 'http://rutherford.com/nulla-natus-facilis-nihil-eveniet-libero-sint-incidunt', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 1, NULL, NULL, NULL),
(2, 'Mr. Abdullah Stehr III', 'Prof. Citlalli Nicolas', NULL, 'Architecto ab qui quia veritatis iusto dolor. Animi aliquid minus voluptatem assumenda ad. Nostrum nihil doloribus non iure.\nVelit incidunt amet ut. Ipsum rerum dicta quod nisi qui quo dolorem sit. Iusto ea perspiciatis aspernatur autem in.', NULL, NULL, NULL, NULL, 8, NULL, NULL, 'false', 'true', 'http://huel.com/aut-architecto-nihil-libero-quam', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 1, NULL, NULL, NULL),
(3, 'Dr. Jed Sanford', 'Kayli Raynor', NULL, 'Commodi quidem perferendis et qui facere est. Corporis delectus qui enim odit in. Voluptatem consequatur quod sit non. Et incidunt temporibus nobis cum ab non ad.', NULL, NULL, NULL, NULL, 3, NULL, NULL, 'true', 'false', 'https://halvorson.com/atque-sit-sequi-ipsa-tempore-at-repellendus-id.html', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 1, NULL, NULL, NULL),
(4, 'Verda Ernser', 'Horacio Emard', NULL, 'Et vel dolorum iure nihil. Dolor ipsa quia dolorum et odio harum. Ipsa voluptas magnam modi in consequatur sit. Nihil ad beatae aut enim beatae omnis.', NULL, NULL, NULL, NULL, 2, NULL, NULL, 'false', 'true', 'https://hayes.info/hic-consequatur-non-voluptatum-facere.html', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 19, 1, 1, 2),
(5, 'Davonte Bailey', 'Mr. Shawn Harvey', NULL, 'Ut consequatur nihil incidunt a eos explicabo qui. Aut ut voluptas sed culpa ducimus asperiores. Non perspiciatis enim earum. Tempora quasi tempore doloribus esse.\nVel praesentium vero magnam. Qui in quibusdam est blanditiis molestiae ut.', NULL, NULL, NULL, NULL, 4, NULL, NULL, 'false', 'false', 'https://www.barton.com/est-quae-officia-esse-quidem-quis-quia', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 19, 1, 1, 2),
(6, 'Claud Heathcote', 'Rylan Stamm', NULL, 'Et est in rerum autem. Sit enim dolores voluptatem temporibus et enim facere. Facere labore odio dicta repellat. Deserunt ut nostrum repellat consequatur placeat.\nAperiam sint qui ex quibusdam dignissimos. Aliquid quia id sunt est rerum. Aut nihil commodi rerum non.', NULL, NULL, NULL, NULL, 7, NULL, NULL, 'true', 'true', 'http://www.cole.com/repellendus-mollitia-nesciunt-iure', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 19, 1, 1, 2),
(7, 'Prof. Hollis Von', 'Leonie Lakin', NULL, 'Ipsum omnis debitis officia iusto molestias. Voluptas aut sunt adipisci quas consequatur. Nobis nobis natus ut quia ullam delectus.', NULL, NULL, NULL, NULL, 7, NULL, NULL, 'false', 'false', 'http://purdy.com/qui-ut-aut-fugit-et-exercitationem.html', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 19, 1, 1, 2),
(8, 'Carolanne Johnston II', 'Dr. Kareem McClure V', NULL, 'Exercitationem aut nobis dolore possimus maiores ut. Enim quaerat voluptatem odit dolorum incidunt culpa qui. Ut veniam temporibus ea sit qui dolores consequatur. Nisi quos qui doloribus vitae.', NULL, NULL, NULL, NULL, 7, NULL, NULL, 'false', 'false', 'http://www.barrows.com/eos-saepe-similique-quo', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 19, 1, 1, 2),
(9, 'Name Altenwerth', 'Mrs. Duane Powlowski PhD', NULL, 'Sunt mollitia beatae magnam totam deleniti. Rem recusandae sit maxime possimus soluta deserunt. Vero aspernatur blanditiis reprehenderit in dignissimos natus quaerat. Voluptate eligendi enim nobis inventore dolorum qui.', NULL, NULL, NULL, NULL, 4, NULL, NULL, 'false', 'false', 'http://walkerhane.com/cumque-deleniti-consequatur-quia-quod-beatae-et.html', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 20, 2, 3, 4),
(10, 'Mr. Dale Ullrich', 'Ansley Lubowitz', NULL, 'Repellendus eum sed qui iste soluta est. Officiis quae et iste nisi aut possimus. Necessitatibus vel provident velit aliquid repellendus saepe aut.\nDolores voluptas distinctio eos ut eum. Hic sed explicabo voluptatem. Omnis et omnis ut rerum.', NULL, NULL, NULL, NULL, 9, NULL, NULL, 'true', 'false', 'http://www.king.biz/ut-harum-ut-omnis-consequatur', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 20, 2, 3, 4),
(11, 'Rex Torp', 'Michaela Harber', NULL, 'Tenetur exercitationem dolores enim. Quasi beatae minima temporibus iste. Autem velit in animi reprehenderit tenetur voluptas. Ex neque dolores rerum.', NULL, NULL, NULL, NULL, 3, NULL, NULL, 'true', 'false', 'http://www.stoltenberghand.org/soluta-non-dolores-quo-vel-et-et', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 20, 2, 3, 4),
(12, 'Mr. Lemuel Casper', 'Enola Mann', NULL, 'Placeat quo iure consectetur vitae minima. Facilis nihil dolorem aperiam corporis saepe dolorum. Cum tempora qui quia. Suscipit ut pariatur dolorem aut cumque voluptates.', NULL, NULL, NULL, NULL, 9, NULL, NULL, 'false', 'false', 'http://www.crona.biz/molestiae-harum-consequuntur-qui-et.html', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 20, 2, 3, 4),
(13, 'Olen Kuphal', 'Kay Torp', NULL, 'Occaecati eos fuga voluptatem velit sunt velit sed. Fuga qui dicta et ut corrupti earum.', NULL, NULL, NULL, NULL, 9, NULL, NULL, 'true', 'true', 'http://www.stoltenberg.com/', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 20, 2, 3, 4),
(14, 'Dr. Dane Cassin', 'Giovanny Hand', NULL, 'Suscipit qui voluptatem voluptates. Et eum aut aperiam id perspiciatis. Non quibusdam at et et velit corrupti minus.', NULL, NULL, NULL, NULL, 3, NULL, NULL, 'true', 'true', 'http://toybednar.com/tempora-quia-labore-laborum-sequi', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 21, 3, 5, 6),
(15, 'Kari Pfeffer', 'Margie Schumm', NULL, 'Eos error officia id laboriosam perferendis incidunt recusandae et. Fuga veritatis sunt et occaecati reiciendis. Quam omnis officia debitis delectus voluptas mollitia. Quo et dolore quisquam sint nihil tempore dolore.', NULL, NULL, NULL, NULL, 8, NULL, NULL, 'false', 'true', 'http://www.schmittsanford.com/', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 21, 3, 5, 6),
(16, 'Meghan Hackett', 'Mrs. Dejah Leuschke', NULL, 'Ut id excepturi vel quod in quae incidunt. Sapiente eos accusamus dolores maiores qui. Est odit nihil architecto iure libero eum eum dignissimos.', NULL, NULL, NULL, NULL, 2, NULL, NULL, 'true', 'true', 'https://www.mccullough.com/assumenda-ipsum-quos-eligendi-quaerat-veritatis-eos', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 21, 3, 5, 6),
(17, 'Prof. Reggie Klocko', 'Mrs. Margarete Stamm', NULL, 'Libero dolorum molestiae voluptatem quaerat nobis voluptate nulla. Deserunt ea repellat sit eius aspernatur. Similique eveniet labore quae optio commodi sed. Nostrum non occaecati accusamus pariatur eos aut illo.', NULL, NULL, NULL, NULL, 7, NULL, NULL, 'false', 'false', 'http://grant.com/', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 21, 3, 5, 6),
(18, 'Reed Nader', 'Ellen Schiller', NULL, 'Enim cumque nihil est corporis. Asperiores repudiandae aliquid delectus soluta. Soluta sit natus dolor eveniet aut debitis autem omnis. Ipsum officia reiciendis est fugit autem illo molestias.', NULL, NULL, NULL, NULL, 0, NULL, NULL, 'true', 'true', 'http://www.harber.com/quaerat-earum-error-qui', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 21, 3, 5, 6),
(19, 'Ms. Florence Ledner II', 'Claude Considine', NULL, 'Quia voluptate voluptas ratione delectus qui odit. Officiis quia vel aliquam quis. Et rerum earum nam et et optio animi.\nDolores quos dolorum voluptatem natus deserunt itaque. Unde odit eveniet autem blanditiis officiis ducimus. Iste aliquid aspernatur omnis adipisci quasi.', NULL, NULL, NULL, NULL, 8, NULL, NULL, 'false', 'true', 'http://metz.com/maxime-quis-atque-unde-magni-veniam', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 1, NULL, NULL, NULL),
(20, 'Jeanie McGlynn MD', 'Ayana Champlin II', NULL, 'Animi aut commodi modi eligendi in consectetur hic ex. Dicta officiis perferendis eos perferendis occaecati reiciendis. Aperiam sunt ab consequuntur repellendus doloribus voluptate. Non quibusdam accusamus quis fugit impedit porro.', NULL, NULL, NULL, NULL, 3, NULL, NULL, 'true', 'false', 'http://hodkiewicz.com/at-quasi-quas-odit-atque-natus-impedit', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 1, NULL, NULL, NULL),
(21, 'Grady Gaylord', 'Modesto Walter', NULL, 'Nihil voluptatum consequatur temporibus voluptatum est. Est voluptas quia assumenda deserunt. Repellendus expedita repellat cumque ducimus consequuntur est voluptatem.', NULL, NULL, NULL, NULL, 7, NULL, NULL, 'false', 'false', 'http://kuhnlarkin.com/', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 1, NULL, NULL, NULL),
(22, 'Nella Gislason', 'Ken Wolff Sr.', NULL, 'Quis provident esse unde sunt sunt dolore at. Qui totam quas et eos. Libero quia laborum laborum magni quia molestiae est. Atque non veniam ut atque magni in.\nVoluptatem nisi aut accusamus sunt labore magnam in. Placeat numquam aperiam possimus.', NULL, NULL, NULL, NULL, 3, NULL, NULL, 'false', 'false', 'http://www.kuvalis.com/iure-at-doloremque-nemo-suscipit-qui-in.html', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 1, NULL, NULL, NULL),
(23, 'Mr. Marquis Kuhic II', 'Keshaun Schoen', NULL, 'Et occaecati voluptatum beatae assumenda adipisci voluptas iusto culpa. Ut cupiditate harum voluptate cum quisquam. Adipisci sit illo sapiente sed est sequi.\nEt sit ipsum est iure perspiciatis nostrum. Iusto magnam eum voluptatem quidem enim consequatur. Officia assumenda voluptatem quaerat et.', NULL, NULL, NULL, NULL, 0, NULL, NULL, 'true', 'true', 'http://rodriguezterry.net/error-alias-quo-non-similique-deserunt-qui-blanditiis', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 1, NULL, NULL, NULL),
(24, 'Dorothea Ankunding', 'Rory Rice', NULL, 'Sint vitae dolorem nesciunt beatae. Debitis quis quisquam nostrum eos repellendus.\nAlias sint sit est ex blanditiis qui quam. Rerum accusantium dolorem mollitia voluptas. Quasi totam voluptatem iure ea qui culpa impedit est. Et et quia inventore doloribus aperiam.', NULL, NULL, NULL, NULL, 0, NULL, NULL, 'false', 'false', 'http://damorehills.com/et-incidunt-nemo-inventore-non-rem', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 1, NULL, NULL, NULL),
(25, 'Hunter Farrell', 'Brannon Schmeler', NULL, 'Dolores optio illo sit voluptas dignissimos. A minus dolor ipsum quia. Nisi qui rem atque et nostrum ducimus.\nDoloribus distinctio fugit ipsum accusantium voluptatem. Sed velit id laboriosam magni. Quo maiores et nulla dolor odio blanditiis quibusdam.', NULL, NULL, NULL, NULL, 5, NULL, NULL, 'true', 'false', 'http://wiegand.org/', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 1, NULL, NULL, NULL),
(26, 'Vida Barton', 'Dr. Quincy Daniel', NULL, 'Voluptatem dolorem dolores quis est et consequuntur nulla. Nobis quo iusto dolorem accusamus. Non nihil quae assumenda libero quam velit neque.\nRerum odio ullam qui maxime debitis amet. Aliquid voluptate at quos iusto ut.', NULL, NULL, NULL, NULL, 2, NULL, NULL, 'true', 'true', 'http://bailey.com/sint-qui-voluptas-esse-tenetur-voluptas-sed-minus-laudantium', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 1, NULL, NULL, NULL),
(27, 'Bonnie O\'Kon', 'Odell Rau MD', NULL, 'Adipisci ut aperiam enim perferendis dolorum. Ullam omnis voluptatem ut et officiis optio mollitia. Ipsa voluptates sequi corporis dignissimos fuga. Possimus culpa nisi molestiae neque reiciendis.', NULL, NULL, NULL, NULL, 7, NULL, NULL, 'false', 'true', 'http://howe.com/facilis-esse-quia-modi-animi-repellat-aliquam', '2018-10-16 05:55:54', '2018-10-16 05:55:54', NULL, 1, NULL, NULL, NULL),
(28, 'Gaetano Torphy', 'Dr. Reta Jacobson', NULL, 'Numquam assumenda dolores aspernatur iusto. Sapiente suscipit tempora consectetur et sint ut. Molestias ducimus repudiandae molestiae officia mollitia reiciendis pariatur aut. Aut sed molestiae ut facilis.', NULL, NULL, NULL, NULL, 2, NULL, NULL, 'false', 'true', 'http://monahanhodkiewicz.com/ad-eius-qui-molestias.html', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 1, NULL, NULL, NULL),
(29, 'Jany Johnston Jr.', 'Mrs. Luisa Kozey', NULL, 'A incidunt aut culpa consequatur est. Similique adipisci sint laudantium quia accusantium. Provident maxime sapiente rerum magnam assumenda deserunt reiciendis accusantium. Quis fugit ipsum eum doloribus et sint.', NULL, NULL, NULL, NULL, 6, NULL, NULL, 'false', 'false', 'http://hermann.org/dolores-nihil-aut-reiciendis-harum.html', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 1, NULL, NULL, NULL),
(30, 'Miss Frederique', 'Abdullah Runte', NULL, 'Suscipit minima cumque aut cupiditate enim. Ut porro hic et officiis consequatur rem. Iusto et esse impedit cupiditate voluptatibus commodi quae.\nQuae alias quibusdam possimus et ipsum. Et mollitia aut laborum explicabo labore ad. Modi ducimus cumque fuga et modi. Quisquam quam laborum et rem aut.', NULL, NULL, NULL, NULL, 2, NULL, NULL, 'true', 'true', 'http://labadie.info/tempora-doloribus-itaque-minima-deleniti-ut-molestiae-assumenda', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 1, NULL, NULL, NULL),
(31, 'Prof. Armand Wolf V', 'Georgianna Bailey DDS', NULL, 'Qui optio fugiat porro vero beatae. Necessitatibus numquam ut inventore nisi et sed. Numquam dolor dolorum et et nobis doloremque.', NULL, NULL, NULL, NULL, 1, NULL, NULL, 'true', 'false', 'http://toyjerde.com/est-molestias-quam-sit-veniam-ea.html', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 1, NULL, NULL, NULL),
(32, 'Norma Schamberger', 'Tomasa Hahn', NULL, 'Consectetur earum tempore ea rerum voluptate. Libero cumque quis pariatur sequi et ut commodi. Sed nobis neque ullam voluptatum enim. Delectus blanditiis odit blanditiis beatae sit nemo.', NULL, NULL, NULL, NULL, 2, NULL, NULL, 'false', 'false', 'http://effertzkrajcik.org/velit-pariatur-quia-excepturi-tempore', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 1, NULL, NULL, NULL),
(33, 'Janae Purdy Jr.', 'Miss Alvena Kuhlman III', NULL, 'Aspernatur veniam minima omnis non dolore aut maiores earum. Ut non iusto a rerum qui. Quisquam rerum ea et. Minus facilis tempora quae qui.', NULL, NULL, NULL, NULL, 4, NULL, NULL, 'true', 'true', 'https://carrollblanda.com/ex-excepturi-sequi-labore-numquam.html', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 1, NULL, NULL, NULL),
(34, 'Cordie Moen', 'Prof. Dolly Goyette', NULL, 'Earum ipsum nam beatae quia laborum non. Repudiandae nihil et qui molestias consequatur quis fugit. Consequatur ipsum ut eius ut sit ex. Dolorum tempora nisi optio ipsum assumenda libero.\nVoluptas est magni ullam. Et hic aut molestiae vel et nobis impedit.', NULL, NULL, NULL, NULL, 0, NULL, NULL, 'false', 'false', 'http://ratke.biz/tempore-rem-sunt-aliquam-placeat', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 1, NULL, NULL, NULL),
(35, 'Christophe Schiller', 'Minerva Bartell', NULL, 'Ullam incidunt quisquam odit minima tempore nisi rerum quis. Atque voluptates labore optio deleniti suscipit dignissimos eius. Placeat beatae eum facilis qui consectetur.', NULL, NULL, NULL, NULL, 5, NULL, NULL, 'false', 'true', 'http://schaefer.com/dolorem-non-sapiente-molestiae-eum', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 1, NULL, NULL, NULL),
(36, 'Melyssa Connelly I', 'Bessie Ortiz', NULL, 'Aut unde et doloremque et provident. Rerum consequatur atque iusto modi alias. Ut sed officia delectus tempora. Vero voluptatum voluptatem nisi voluptatum sunt.\nPossimus rem et reiciendis tempora et est sunt quia. Non nam culpa molestiae et et quibusdam debitis.', NULL, NULL, NULL, NULL, 1, NULL, NULL, 'false', 'false', 'http://homenick.com/ut-quisquam-enim-accusantium-consequuntur-inventore-necessitatibus-odio', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 1, NULL, NULL, NULL),
(37, 'Mr. Jettie Ledner PhD', 'Nathen Hickle DVM', NULL, 'Ut quisquam quaerat alias ut commodi accusamus. Perspiciatis consequatur minus nemo exercitationem et. Sed at sapiente iusto itaque est totam id odit.', NULL, NULL, NULL, NULL, 1, NULL, NULL, 'true', 'false', 'https://crooks.org/quos-saepe-voluptas-laboriosam-delectus-et-voluptas-fugiat-libero.html', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 1, NULL, NULL, NULL),
(38, 'Jaquelin Braun', 'Reed Hermiston', NULL, 'Consequuntur aperiam accusamus eos sunt et est fugit unde. Autem ut consequatur molestiae nam. Est accusantium eum laboriosam consequatur. Nam beatae totam autem et.', NULL, NULL, NULL, NULL, 8, NULL, NULL, 'false', 'true', 'http://www.fritsch.com/voluptatem-placeat-corrupti-et-harum-modi-iure-consequatur.html', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 1, NULL, NULL, NULL),
(39, 'Mateo Bradtke', 'Stewart Botsford Sr.', NULL, 'Illum esse labore sint eaque repellendus. Deserunt deserunt omnis in consequatur. Quaerat ratione perspiciatis incidunt deleniti.\nMolestiae velit est architecto tempora. Inventore sunt sed est dignissimos. Esse temporibus nihil error ex quae ex nobis.', NULL, NULL, NULL, NULL, 0, NULL, NULL, 'true', 'true', 'http://www.conn.biz/', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 1, NULL, NULL, NULL),
(40, 'Stephon White', 'Jaylon Waters', NULL, 'Et voluptatum repudiandae voluptatem. Dolores qui tempora eum quas culpa sed iusto.', NULL, NULL, NULL, NULL, 3, NULL, NULL, 'false', 'true', 'http://www.mccullough.org/', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 1, NULL, NULL, NULL),
(41, 'Prof. Jana Anderson IV', 'Lloyd Bogan', NULL, 'Maxime debitis fugit corporis architecto consequuntur aut. Facere reiciendis quo aut iste et facilis. Odio eos dolore voluptatem non.\nDolores doloremque ut dignissimos maxime. Quia et earum accusamus. Sunt nesciunt laudantium saepe omnis nulla ipsam. Et dolore excepturi voluptas.', NULL, NULL, NULL, NULL, 3, NULL, NULL, 'true', 'true', 'http://doyle.org/sunt-accusantium-qui-aut-explicabo-placeat-aliquid.html', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_recommends_products`
--

CREATE TABLE `shop_recommends_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `recommends` enum('1','2','3','4','5') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '5',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `shop_product_id` int(10) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `shop_recommends_products`
--

INSERT INTO `shop_recommends_products` (`id`, `recommends`, `created_at`, `updated_at`, `deleted_at`, `shop_product_id`) VALUES
(1, '3', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 1),
(2, '5', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 1),
(3, '5', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 1),
(4, '3', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 1),
(5, '2', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 1),
(6, '4', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 1),
(7, '1', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 1),
(8, '5', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 1),
(9, '1', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 1),
(10, '5', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 1),
(11, '5', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 29),
(12, '1', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 29),
(13, '2', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 29),
(14, '5', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 29),
(15, '3', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 29),
(16, '2', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 29),
(17, '5', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 29),
(18, '2', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 29),
(19, '5', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 29),
(20, '2', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 29),
(21, '2', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 30),
(22, '3', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 30),
(23, '2', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 30),
(24, '5', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 30),
(25, '3', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 30),
(26, '4', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 30),
(27, '2', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 30),
(28, '2', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 30),
(29, '3', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 30),
(30, '3', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 30),
(31, '4', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 31),
(32, '3', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 31),
(33, '3', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 31),
(34, '5', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 31),
(35, '5', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 31),
(36, '1', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 31),
(37, '3', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 31),
(38, '1', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 31),
(39, '2', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 31),
(40, '5', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 31),
(41, '4', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 32),
(42, '2', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 32),
(43, '1', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 32),
(44, '2', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 32),
(45, '4', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 32),
(46, '5', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 32),
(47, '2', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 32),
(48, '1', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 32),
(49, '4', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 32),
(50, '4', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 32),
(51, '2', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 33),
(52, '4', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 33),
(53, '4', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 33),
(54, '1', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 33),
(55, '1', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 33),
(56, '4', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 33),
(57, '1', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 33),
(58, '2', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 33),
(59, '4', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 33),
(60, '2', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 33),
(61, '3', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 34),
(62, '3', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 34),
(63, '3', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 34),
(64, '5', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 34),
(65, '3', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 34),
(66, '3', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 34),
(67, '3', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 34),
(68, '3', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 34),
(69, '4', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 34),
(70, '3', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 34),
(71, '4', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 35),
(72, '2', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 35),
(73, '2', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 35),
(74, '5', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 35),
(75, '4', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 35),
(76, '5', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 35),
(77, '1', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 35),
(78, '4', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 35),
(79, '4', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 35),
(80, '5', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 35),
(81, '2', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 36),
(82, '5', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 36),
(83, '2', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 36),
(84, '4', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 36),
(85, '2', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 36),
(86, '3', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 36),
(87, '3', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 36),
(88, '3', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 36),
(89, '1', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 36),
(90, '3', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 36),
(91, '2', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 37),
(92, '4', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 37),
(93, '5', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 37),
(94, '2', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 37),
(95, '5', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 37),
(96, '2', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 37),
(97, '1', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 37),
(98, '4', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 37),
(99, '5', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 37),
(100, '3', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 37),
(101, '2', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 38),
(102, '5', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 38),
(103, '5', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 38),
(104, '3', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 38),
(105, '3', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 38),
(106, '3', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 38),
(107, '4', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 38),
(108, '3', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 38),
(109, '2', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 38),
(110, '1', '2018-10-16 05:55:55', '2018-10-16 05:55:55', NULL, 38);

-- --------------------------------------------------------

--
-- Структура таблицы `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `translations`
--

INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
(1, 'data_types', 'display_name_singular', 5, 'pt', 'Post', '2018-10-17 02:59:04', '2018-10-17 02:59:04'),
(2, 'data_types', 'display_name_singular', 6, 'pt', 'Página', '2018-10-17 02:59:04', '2018-10-17 02:59:04'),
(3, 'data_types', 'display_name_singular', 1, 'pt', 'Utilizador', '2018-10-17 02:59:04', '2018-10-17 02:59:04'),
(4, 'data_types', 'display_name_singular', 4, 'pt', 'Categoria', '2018-10-17 02:59:04', '2018-10-17 02:59:04'),
(5, 'data_types', 'display_name_singular', 2, 'pt', 'Menu', '2018-10-17 02:59:04', '2018-10-17 02:59:04'),
(6, 'data_types', 'display_name_singular', 3, 'pt', 'Função', '2018-10-17 02:59:04', '2018-10-17 02:59:04'),
(7, 'data_types', 'display_name_plural', 5, 'pt', 'Posts', '2018-10-17 02:59:04', '2018-10-17 02:59:04'),
(8, 'data_types', 'display_name_plural', 6, 'pt', 'Páginas', '2018-10-17 02:59:04', '2018-10-17 02:59:04'),
(9, 'data_types', 'display_name_plural', 1, 'pt', 'Utilizadores', '2018-10-17 02:59:04', '2018-10-17 02:59:04'),
(10, 'data_types', 'display_name_plural', 4, 'pt', 'Categorias', '2018-10-17 02:59:04', '2018-10-17 02:59:04'),
(11, 'data_types', 'display_name_plural', 2, 'pt', 'Menus', '2018-10-17 02:59:04', '2018-10-17 02:59:04'),
(12, 'data_types', 'display_name_plural', 3, 'pt', 'Funções', '2018-10-17 02:59:04', '2018-10-17 02:59:04'),
(13, 'categories', 'slug', 1, 'pt', 'categoria-1', '2018-10-17 02:59:04', '2018-10-17 02:59:04'),
(14, 'categories', 'name', 1, 'pt', 'Categoria 1', '2018-10-17 02:59:04', '2018-10-17 02:59:04'),
(15, 'categories', 'slug', 2, 'pt', 'categoria-2', '2018-10-17 02:59:04', '2018-10-17 02:59:04'),
(16, 'categories', 'name', 2, 'pt', 'Categoria 2', '2018-10-17 02:59:04', '2018-10-17 02:59:04'),
(17, 'pages', 'title', 1, 'pt', 'Olá Mundo', '2018-10-17 02:59:04', '2018-10-17 02:59:04'),
(18, 'pages', 'slug', 1, 'pt', 'ola-mundo', '2018-10-17 02:59:04', '2018-10-17 02:59:04'),
(19, 'pages', 'body', 1, 'pt', '<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', '2018-10-17 02:59:04', '2018-10-17 02:59:04'),
(20, 'menu_items', 'title', 1, 'pt', 'Painel de Controle', '2018-10-17 02:59:04', '2018-10-17 02:59:04'),
(21, 'menu_items', 'title', 2, 'pt', 'Media', '2018-10-17 02:59:04', '2018-10-17 02:59:04'),
(22, 'menu_items', 'title', 12, 'pt', 'Publicações', '2018-10-17 02:59:04', '2018-10-17 02:59:04'),
(23, 'menu_items', 'title', 3, 'pt', 'Utilizadores', '2018-10-17 02:59:04', '2018-10-17 02:59:04'),
(24, 'menu_items', 'title', 11, 'pt', 'Categorias', '2018-10-17 02:59:04', '2018-10-17 02:59:04'),
(25, 'menu_items', 'title', 13, 'pt', 'Páginas', '2018-10-17 02:59:04', '2018-10-17 02:59:04'),
(26, 'menu_items', 'title', 4, 'pt', 'Funções', '2018-10-17 02:59:04', '2018-10-17 02:59:04'),
(27, 'menu_items', 'title', 5, 'pt', 'Ferramentas', '2018-10-17 02:59:04', '2018-10-17 02:59:04'),
(28, 'menu_items', 'title', 6, 'pt', 'Menus', '2018-10-17 02:59:04', '2018-10-17 02:59:04'),
(29, 'menu_items', 'title', 7, 'pt', 'Base de dados', '2018-10-17 02:59:04', '2018-10-17 02:59:04'),
(30, 'menu_items', 'title', 10, 'pt', 'Configurações', '2018-10-17 02:59:04', '2018-10-17 02:59:04');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'Admin', 'admin@admin.com', 'users/default.png', NULL, '$2y$10$xj33IXUWEdcSEkYtovehze/dAkKiEi3POOQ26KeC0wxowtGplHy2C', 'nGbeJ0u0qS408VzBru4Befxm0L0FCTEnph45B5CqGW5u5U0wg8kfLqUMHf2R', NULL, '2018-10-17 02:59:04', '2018-10-17 02:59:04');

-- --------------------------------------------------------

--
-- Структура таблицы `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Индексы таблицы `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Индексы таблицы `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Индексы таблицы `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Индексы таблицы `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Индексы таблицы `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Индексы таблицы `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`);

--
-- Индексы таблицы `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Индексы таблицы `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Индексы таблицы `shop_additional`
--
ALTER TABLE `shop_additional`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `shop_attr`
--
ALTER TABLE `shop_attr`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `shop_attr_val`
--
ALTER TABLE `shop_attr_val`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shop_attr_val_shop_attr_id_foreign` (`shop_attr_id`),
  ADD KEY `shop_attr_val_shop_product_id_foreign` (`shop_product_id`);

--
-- Индексы таблицы `shop_category`
--
ALTER TABLE `shop_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shop_category_parent_id_foreign` (`parent_id`);

--
-- Индексы таблицы `shop_category_attr`
--
ALTER TABLE `shop_category_attr`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shop_category_attr_parent_id_foreign` (`parent_id`),
  ADD KEY `shop_category_attr_shop_category_id_foreign` (`shop_category_id`),
  ADD KEY `shop_category_attr_shop_attr_id_foreign` (`shop_attr_id`);

--
-- Индексы таблицы `shop_clients`
--
ALTER TABLE `shop_clients`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `shop_comments`
--
ALTER TABLE `shop_comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shop_comments_shop_product_id_foreign` (`shop_product_id`);

--
-- Индексы таблицы `shop_gallaries`
--
ALTER TABLE `shop_gallaries`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `shop_info`
--
ALTER TABLE `shop_info`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `shop_jobs`
--
ALTER TABLE `shop_jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shop_jobs_queue_reserved_reserved_at_index` (`queue`,`reserved`,`reserved_at`);

--
-- Индексы таблицы `shop_npcity`
--
ALTER TABLE `shop_npcity`
  ADD KEY `shop_npcity_name_index` (`name`);

--
-- Индексы таблицы `shop_npunit`
--
ALTER TABLE `shop_npunit`
  ADD KEY `shop_npunit_name_index` (`name`);

--
-- Индексы таблицы `shop_orders`
--
ALTER TABLE `shop_orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shop_orders_shop_client_id_foreign` (`shop_client_id`);

--
-- Индексы таблицы `shop_order_add`
--
ALTER TABLE `shop_order_add`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shop_order_add_shop_order_id_foreign` (`shop_order_id`),
  ADD KEY `shop_order_add_shop_additional_id_foreign` (`shop_additional_id`);

--
-- Индексы таблицы `shop_order_files`
--
ALTER TABLE `shop_order_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shop_order_files_shop_order_id_foreign` (`shop_order_id`);

--
-- Индексы таблицы `shop_order_items`
--
ALTER TABLE `shop_order_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shop_order_items_shop_order_id_foreign` (`shop_order_id`),
  ADD KEY `shop_order_items_shop_product_id_foreign` (`shop_product_id`);

--
-- Индексы таблицы `shop_products`
--
ALTER TABLE `shop_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shop_products_category_id_foreign` (`category_id`),
  ADD KEY `shop_products_parent_id_foreign` (`parent_id`),
  ADD KEY `shop_products_shop_gallery_id_foreign` (`shop_gallery_id`),
  ADD KEY `shop_products_shop_gallery_back_id_foreign` (`shop_gallery_back_id`);

--
-- Индексы таблицы `shop_recommends_products`
--
ALTER TABLE `shop_recommends_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shop_recommends_products_shop_product_id_foreign` (`shop_product_id`);

--
-- Индексы таблицы `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Индексы таблицы `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT для таблицы `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT для таблицы `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT для таблицы `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `shop_additional`
--
ALTER TABLE `shop_additional`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `shop_attr`
--
ALTER TABLE `shop_attr`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `shop_attr_val`
--
ALTER TABLE `shop_attr_val`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT для таблицы `shop_category`
--
ALTER TABLE `shop_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT для таблицы `shop_category_attr`
--
ALTER TABLE `shop_category_attr`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `shop_clients`
--
ALTER TABLE `shop_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT для таблицы `shop_comments`
--
ALTER TABLE `shop_comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;

--
-- AUTO_INCREMENT для таблицы `shop_gallaries`
--
ALTER TABLE `shop_gallaries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT для таблицы `shop_info`
--
ALTER TABLE `shop_info`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `shop_jobs`
--
ALTER TABLE `shop_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `shop_orders`
--
ALTER TABLE `shop_orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT для таблицы `shop_order_add`
--
ALTER TABLE `shop_order_add`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `shop_order_files`
--
ALTER TABLE `shop_order_files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT для таблицы `shop_order_items`
--
ALTER TABLE `shop_order_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `shop_products`
--
ALTER TABLE `shop_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT для таблицы `shop_recommends_products`
--
ALTER TABLE `shop_recommends_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;

--
-- AUTO_INCREMENT для таблицы `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `shop_attr_val`
--
ALTER TABLE `shop_attr_val`
  ADD CONSTRAINT `shop_attr_val_shop_attr_id_foreign` FOREIGN KEY (`shop_attr_id`) REFERENCES `shop_attr` (`id`),
  ADD CONSTRAINT `shop_attr_val_shop_product_id_foreign` FOREIGN KEY (`shop_product_id`) REFERENCES `shop_products` (`id`);

--
-- Ограничения внешнего ключа таблицы `shop_category`
--
ALTER TABLE `shop_category`
  ADD CONSTRAINT `shop_category_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `shop_category` (`id`);

--
-- Ограничения внешнего ключа таблицы `shop_category_attr`
--
ALTER TABLE `shop_category_attr`
  ADD CONSTRAINT `shop_category_attr_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `shop_category_attr` (`id`),
  ADD CONSTRAINT `shop_category_attr_shop_attr_id_foreign` FOREIGN KEY (`shop_attr_id`) REFERENCES `shop_attr` (`id`),
  ADD CONSTRAINT `shop_category_attr_shop_category_id_foreign` FOREIGN KEY (`shop_category_id`) REFERENCES `shop_category` (`id`);

--
-- Ограничения внешнего ключа таблицы `shop_comments`
--
ALTER TABLE `shop_comments`
  ADD CONSTRAINT `shop_comments_shop_product_id_foreign` FOREIGN KEY (`shop_product_id`) REFERENCES `shop_products` (`id`);

--
-- Ограничения внешнего ключа таблицы `shop_orders`
--
ALTER TABLE `shop_orders`
  ADD CONSTRAINT `shop_orders_shop_client_id_foreign` FOREIGN KEY (`shop_client_id`) REFERENCES `shop_clients` (`id`);

--
-- Ограничения внешнего ключа таблицы `shop_order_add`
--
ALTER TABLE `shop_order_add`
  ADD CONSTRAINT `shop_order_add_shop_additional_id_foreign` FOREIGN KEY (`shop_additional_id`) REFERENCES `shop_additional` (`id`),
  ADD CONSTRAINT `shop_order_add_shop_order_id_foreign` FOREIGN KEY (`shop_order_id`) REFERENCES `shop_orders` (`id`);

--
-- Ограничения внешнего ключа таблицы `shop_order_files`
--
ALTER TABLE `shop_order_files`
  ADD CONSTRAINT `shop_order_files_shop_order_id_foreign` FOREIGN KEY (`shop_order_id`) REFERENCES `shop_orders` (`id`);

--
-- Ограничения внешнего ключа таблицы `shop_order_items`
--
ALTER TABLE `shop_order_items`
  ADD CONSTRAINT `shop_order_items_shop_order_id_foreign` FOREIGN KEY (`shop_order_id`) REFERENCES `shop_orders` (`id`),
  ADD CONSTRAINT `shop_order_items_shop_product_id_foreign` FOREIGN KEY (`shop_product_id`) REFERENCES `shop_products` (`id`);

--
-- Ограничения внешнего ключа таблицы `shop_products`
--
ALTER TABLE `shop_products`
  ADD CONSTRAINT `shop_products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `shop_category` (`id`),
  ADD CONSTRAINT `shop_products_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `shop_products` (`id`),
  ADD CONSTRAINT `shop_products_shop_gallery_back_id_foreign` FOREIGN KEY (`shop_gallery_back_id`) REFERENCES `shop_gallaries` (`id`),
  ADD CONSTRAINT `shop_products_shop_gallery_id_foreign` FOREIGN KEY (`shop_gallery_id`) REFERENCES `shop_gallaries` (`id`);

--
-- Ограничения внешнего ключа таблицы `shop_recommends_products`
--
ALTER TABLE `shop_recommends_products`
  ADD CONSTRAINT `shop_recommends_products_shop_product_id_foreign` FOREIGN KEY (`shop_product_id`) REFERENCES `shop_products` (`id`);

--
-- Ограничения внешнего ключа таблицы `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Ограничения внешнего ключа таблицы `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
