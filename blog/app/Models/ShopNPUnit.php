<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopNPUnit extends Model
{
    protected $table='shop_npunit';
    public $timestamps = false;
}
