<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopGallery extends Model
{
    protected $table='shop_gallaries';

    public function product(){
        return $this->belongsTo('App\Models\ShopProduct', 'shop_gallery_id', 'id');
    }

    public function product_back(){
        return $this->belongsTo('App\Models\ShopProduct', 'shop_gallery_back_id', 'id');
    }
}
