<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopClient extends Model
{
    protected $table='shop_clients';

    public function order(){
        return $this->belongsTo('App\Models\ShopOrder','shop_client_id','id');
    }
}
