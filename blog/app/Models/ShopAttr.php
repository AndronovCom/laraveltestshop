<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopAttr extends Model
{
    protected $table='shop_attr';

    public function attrVal(){
        return $this->belongsTo('App\Models\ShopAttrVal','shop_attr_id','id');
    }

    public function category(){
        return $this->belongsToMany('App\Models\ShopCategory', 'shop_category_attr', 'shop_attr_id','id');
    }
}
