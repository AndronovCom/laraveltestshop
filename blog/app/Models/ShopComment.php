<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopComment extends Model
{
    protected $table='shop_comments';

    public function product(){
        return $this->hasOne('App\Models\ShopComment', 'id', 'shop_product_id');
    }
}
