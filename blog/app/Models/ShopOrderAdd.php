<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopOrderAdd extends Model
{
    protected $table='shop_order_add';

    public function orders(){
        return $this->hasMany('App\Models\ShopOrder','id','shop_order_id');
    }

    public function additional(){
        return $this->hasMany('App\Models\ShopAdditional','id','shop_additional_id');
    }



}
