<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopProduct extends Model
{
    protected $table='shop_products';

    public function category(){
        return $this->hasOne('App\Models\ShopCategory', 'id', 'category_id');
    }

    public function orders(){
        return $this->belongsToMany('App\Models\ShopOrder','shop_order_items','shop_product_id','id');
    }

    public function gallery(){
        return $this->hasMany('App\Models\ShopGallery','id','shop_gallery_id');
    }

    public function gallery_back(){
        return $this->hasMany('App\Models\ShopGallery','id','shop_gallery_back_id');
    }
}
