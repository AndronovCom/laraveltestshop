<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopNPCity extends Model
{
    protected $table='shop_npcity';
    public $timestamps = false;
}
