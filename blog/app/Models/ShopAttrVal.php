<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopAttrVal extends Model
{
    protected $table='shop_attr_val';

    public function attr(){
        return $this->hasMany('App\Models\ShopAttr','id','shop_attr_id');
    }

    public function product(){
            return $this->hasMany('App\Models\ShopProduct','id','shop_product_id');
    }
}
