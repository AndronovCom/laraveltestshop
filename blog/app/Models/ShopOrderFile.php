<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopOrderFile extends Model
{
   protected  $table='shop_order_files';

    public function order(){
        return $this->hasOne('App\Models\ShopOrder','id','shop_order_id');
    }
}
