<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopOrder extends Model
{
    protected $table='shop_orders';

    public function client(){
        return $this->hasOne('App\Models\ShopClient','id','shop_client_id');
    }

    public function additional(){
        return $this->belongsToMany('App\Models\ShopAdditional','shop_order_add','shop_order_id','id');
    }

    public function products(){
        return $this->belongsToMany('App\Models\ShopProduct','shop_order_items','shop_order_id','id');
    }
}
