<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopAdditional extends Model
{
    protected $table='shop_additional';

    public function orders(){
        return $this->belongsToMany('App\Models\ShopOrder','shop_order_add','shop_additional_id','id');
    }
}

