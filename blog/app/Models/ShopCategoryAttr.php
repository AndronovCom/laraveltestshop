<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopCategoryAttr extends Model
{
    protected $table='shop_category_attr';


    public function category_attr(){
        return $this->belongsTo('App\Models\ShopCategoryAttr', 'parent_id','id');
    }

    public function category(){
        return $this->hasMany('App\Models\ShopCategory', 'shop_category_id', 'id');
    }

    public function attribute(){
        return $this->hasMany('App\Models\ShopAttr', 'shop_attr_id', 'id');
    }
}
