<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopOrderItems extends Model
{
    protected  $table='shop_order_items';

    public function orders(){
        return $this->hasMany('App\Models\ShopOrder','id','shop_order_id');
    }

    public function protducts(){
        return $this->hasMany('App\Models\ShopProduct','id','shop_product_id');
    }
}
