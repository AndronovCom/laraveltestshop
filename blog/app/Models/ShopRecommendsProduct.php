<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopRecommendsProduct extends Model
{
    protected $table='shop_recommends_products';

    public function product(){
        return $this->hasOne('App\Models\ShopProduct', 'id', 'shop_product_id');
    }
}
