<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopCategory extends Model
{
    protected $table='shop_category';

    public function attribure(){
        return $this->belongsToMany('App\Models\ShopAttr','shop_category_attr', 'shop_category_id','id');
    }

    public function category(){
        return $this->hasMany('App\Models\ShopCategory', 'parent_id', 'id');
    }
}
