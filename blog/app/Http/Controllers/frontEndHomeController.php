<?php

namespace App\Http\Controllers;

use App\Models\ShopGallery;
use Illuminate\Http\Request;
use  App\Models\ShopProduct;
use Illuminate\Support\Facades\DB;

class frontEndHomeController extends Controller
{
    public function index(){
        $productsAll=ShopProduct::get();
        $gallaryAll=[];
        $gallariesBack=[];
        foreach ($productsAll as $product){
            $gallaryAll[$product->id]=DB::table('shop_gallaries')->where('id',$product->shop_gallery_id)->get()->first();
            $gallariesBack[$product->id]=DB::table('shop_gallaries')->where('id',$product->shop_gallery_back_id)->get()->first();
        }
        $gallaryAll = array_where($gallaryAll, function ($value, $key) {
            return isset($value);
        });
        foreach ($gallaryAll as $key=>$value){
            $gallaryAll[$key]=(array)$value;
        }
        $gallariesBack = array_where($gallariesBack, function ($value, $key) {
            return isset($value);
        });
        foreach ($gallariesBack as $key=>$value){
            $gallariesBack[$key]=(array)$value;
        }
        return view('frontEnd.home.index',compact('productsAll','gallaryAll','gallariesBack'));
    }
}
