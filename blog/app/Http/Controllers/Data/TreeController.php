<?php

namespace App\Http\Controllers\Data;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ShopCategory;

class TreeController extends Controller
{
    public function treeView()
    {
        $Categorys = ShopCategory::where('parent_id', '=', null)->get();
        $tree='<ul id="browser">';
        foreach ($Categorys as $Category) {
            $tree .='<li class="tree-view closed"<a class="tree-name">'.$Category->title.'</a>';
            if(count($Category->category)) {
                $tree .=$this->childView($Category);
            }
        }
        $tree .='</ul>';
        return view('welcome',compact('tree'));
    }


    public function childView($Category)
    {
        $html ='<ul>';
        foreach ($Category->category as $arr) {
            if(count($arr->category)){
                $html .='<li><a class="tree-name">'.$arr->title.'</a>';
                $html.= $this->childView($arr);
            }else{
                $html .='<li><a class="tree-name">'.$arr->title.'</a>';
                $html .="</li>";
            }
        }
        $html .="</ul>";
        return $html;
    }
}
