<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
    @include('frontEnd.includes.head')
    @yield('stylesheet')
    @include('frontEnd.includes.script')
</head>
<body>
@include('frontEnd.includes.nav')
@yield('content')
@include('frontEnd.includes.footer')
@yield('script')
</body>
</html>