<?php

use Illuminate\Database\Seeder;

class ShopAttrSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\ShopAttr::class,5)->create();
    }
}
