<?php

use Illuminate\Database\Seeder;

class ShopNPUnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\ShopNPUnit::class,10)->create();
    }
}
