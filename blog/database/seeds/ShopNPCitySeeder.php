<?php

use Illuminate\Database\Seeder;

class ShopNPCitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\ShopNPCity::class,10)->create();
    }
}
