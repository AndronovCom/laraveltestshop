<?php

use Illuminate\Database\Seeder;

class ShopAttrValSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\ShopAttrVal::class, 3)->create()->each(function ($c) {
            factory(App\Models\ShopAttrVal::class, 5)->create([
                'shop_product_id' =>$c->product()->save(factory(App\Models\ShopProduct::class)->make()),
                'shop_attr_id'=>$c->attr()->save(factory(App\Models\ShopAttr::class)->make())
            ]);
        });
    }
}
