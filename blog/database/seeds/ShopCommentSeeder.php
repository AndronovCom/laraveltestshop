<?php

use Illuminate\Database\Seeder;

class ShopCommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\ShopComment::class,10)->create()->each(function ($c){
            factory(App\Models\ShopComment::class,10)->create([
                'shop_product_id'=>$c->product()->save(factory(App\Models\ShopProduct::class)->make())
            ]);
        });
    }
}
