<?php

use Illuminate\Database\Seeder;

class ShopOrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\ShopOrder::class, 3)->create()->each(function ($c) {
            factory(App\Models\ShopOrder::class, 5)->create([
                'shop_client_id' => $c->client()->save(factory(App\Models\ShopClient::class)->make()),
            ]);
        });
    }
}
