<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(ShopAttrSeeder::class);
        $this->call(ShopCategorySeeder::class);
        $this->call(ShopProductSeeder::class);
        $this->call(ShopCommentSeeder::class);
        $this->call(ShopRecommendsProductSeeder::class);
        $this->call(ShopAttrValSeeder::class);
        $this->call(ShopNPUnitSeeder::class);
        $this->call(ShopNPCitySeeder::class);
        $this->call(ShopGallerySeeder::class);
        $this->call(ShopInfoSeeder::class);
        $this->call(ShopJobSeeder::class);
        $this->call(ShopClientSeeder::class);
        $this->call(ShopOrderSeeder::class);
        $this->call(ShopOrderFileSeeder::class);
        $this->call(ShopAdditionalSeeder::class);
    }
}
