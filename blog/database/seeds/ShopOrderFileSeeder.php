<?php

use Illuminate\Database\Seeder;

class ShopOrderFileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\ShopOrderFile::class, 3)->create()->each(function ($c) {
            factory(App\Models\ShopOrderFile::class, 5)->create([
                'shop_order_id' => $c->order()->save(factory(App\Models\ShopOrder::class)->make()),
            ]);
        });
    }
}
