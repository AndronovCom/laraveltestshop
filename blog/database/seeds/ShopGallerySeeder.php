<?php

use Illuminate\Database\Seeder;

class ShopGallerySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\ShopGallery::class,10)->create();
    }
}
