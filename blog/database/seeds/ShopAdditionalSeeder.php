<?php

use Illuminate\Database\Seeder;

class ShopAdditionalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       factory(App\Models\ShopAdditional::class,10)->create();
    }
}
