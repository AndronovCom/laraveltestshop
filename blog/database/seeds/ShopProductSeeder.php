<?php

use Illuminate\Database\Seeder;
use App\Models\ShopProduct;
class ShopProductSeeder extends Seeder
{
    /**
     * Run the database seeds.s
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\ShopProduct::class, 3)->create()->each(function ($c) {
            factory(App\Models\ShopProduct::class, 5)->create([
                'parent_id' => $c->id,
                'category_id'=>$c->category()->save(factory(App\Models\ShopCategory::class)->make()),
                'shop_gallery_id'=>$c->gallery()->save(factory(App\Models\ShopGallery::class)->make()),
                'shop_gallery_back_id'=>$c->gallery_back()->save(factory(App\Models\ShopGallery::class)->make())
            ]);
        });
    }
}
