<?php

use Illuminate\Database\Seeder;

class ShopInfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\ShopInfo::class,10)->create();

    }
}
