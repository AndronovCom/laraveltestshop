<?php

use Illuminate\Database\Seeder;

class ShopJobSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\ShopJob::class,5)->create();

    }
}
