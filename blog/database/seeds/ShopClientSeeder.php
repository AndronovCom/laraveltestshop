<?php

use Illuminate\Database\Seeder;

class ShopClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\ShopClient::class,10)->create();
    }
}
