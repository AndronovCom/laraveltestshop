<?php

use Illuminate\Database\Seeder;

class ShopCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\ShopCategory::class, 3)->create()->each(function ($c) {
            factory(App\Models\ShopCategory::class, 5)->create([
                'parent_id' => $c->id
            ]);
        });
    }
}
