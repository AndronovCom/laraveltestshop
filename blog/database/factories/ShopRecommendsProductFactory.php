<?php

use Faker\Generator as Faker;

$factory->define(App\Models\ShopRecommendsProduct::class, function (Faker $faker) {
    return [
        'recommends'=>$faker->randomElement(['1','2','3','4','5']),
    ];
});
