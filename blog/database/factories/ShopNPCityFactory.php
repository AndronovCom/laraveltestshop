<?php

use Faker\Generator as Faker;

$factory->define(App\Models\ShopNPCity::class, function (Faker $faker) {
    return [
        'name'=>$faker->unique()->city,
        'ref'=>$faker->url
    ];
});
