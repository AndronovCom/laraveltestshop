<?php

use Faker\Generator as Faker;

$factory->define(App\Models\ShopAttr::class, function (Faker $faker) {
    return [
        'title'=>$faker->name,
        'type'=>$faker->name,
        'unit'=>$faker->text('5'),
    ];
});
