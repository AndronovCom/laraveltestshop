<?php

use Faker\Generator as Faker;

$factory->define(App\Models\ShopOrderFile::class, function (Faker $faker) {
    return [
        'name'=>$faker->city,
        'hash'=>$faker->sha256,
        'mime'=>$faker->mimeType,
        'extension'=>$faker->realText(20),
        'status'=>$faker->randomElement(['tmp','success']),
        'image'=>$faker->randomElement(['true','false']),
    ];
});
