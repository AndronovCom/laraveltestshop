<?php

use Faker\Generator as Faker;

$factory->define(App\Models\ShopProduct::class, function (Faker $faker) {
    $faker=\Faker\Factory::create('Internet');
    //$filePath = dirname(dirname(dirname(__FILE__))).'\public\frontEnd\images_product';
    //if(!File::exists($filePath)){
    //    File::makeDirectory($filePath);
    //}
    return [
        'title'=>$faker->name,
        'name'=>$faker->name,
        'description'=>$faker->text(300),
        'price'=>$faker->randomDigit(100,500),
        'urlhash'=>$faker->url,
        'isset'=>$faker->randomElement(['true','false']),
        'visible'=>$faker->randomElement(['true','false']),
    ];
});
