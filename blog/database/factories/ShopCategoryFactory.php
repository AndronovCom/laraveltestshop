<?php

use Faker\Generator as Faker;

$factory->define(App\Models\ShopCategory::class, function (Faker $faker) {
    $faker=\Faker\Factory::create('Internet');
    return [
        'title'=>$faker->name,
        'description'=>$faker->name,
        'cover'=>$faker->name,
        'icon'=>$faker->name,
        'urlhash'=>$faker->url
    ];
});
