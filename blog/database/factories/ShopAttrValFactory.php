<?php

use Faker\Generator as Faker;

$factory->define(App\Models\ShopAttrVal::class, function (Faker $faker) {
    return [
        'value'=>$faker->name,
    ];
});
