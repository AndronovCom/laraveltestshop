<?php

use Faker\Generator as Faker;

$factory->define(App\Models\ShopAdditional::class, function (Faker $faker) {
    return [
        'name'=>$faker->name,
        'description'=>$faker->realText(20),
        'price'=>$faker->randomDigit(10,500),
    ];
});
