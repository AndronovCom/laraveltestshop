<?php

use Faker\Generator as Faker;

$factory->define(App\Models\ShopOrder::class, function (Faker $faker) {
    return [
        'delivery_city'=>$faker->city,
        'delivery_type'=>$faker->randomElement(['adr','np']),
        'pay_type'=>$faker->randomElement(['null','privat24','privat_terminal','liqpay']),
        'code'=>$faker->countryCode,
        'status'=>$faker->randomElement(['new','paid','sent']),
    ];
});
