<?php

use Faker\Generator as Faker;

$factory->define(App\Models\ShopNPUnit::class, function (Faker $faker) {
    return [
        'name'=>$faker->unique()->name,
        'ref'=>$faker->url
    ];
});
