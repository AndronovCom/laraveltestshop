<?php

use Faker\Generator as Faker;

$factory->define(App\Models\ShopInfo::class, function (Faker $faker) {
    return [
        'text'=>$faker->realText(20),
    ];
});
