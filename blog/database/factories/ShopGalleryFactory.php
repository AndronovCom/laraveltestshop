<?php

use Faker\Generator as Faker;

$factory->define(App\Models\ShopGallery::class, function (Faker $faker) {
    $faker=\Faker\Factory::create('File');
    return [
        'filename'=>$faker->imageUrl(300, 400),
    ];
});
