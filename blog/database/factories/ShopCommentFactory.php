<?php

use Faker\Generator as Faker;

$factory->define(App\Models\ShopComment::class, function (Faker $faker) {
    return [
        'name'=>$faker->name,
        'email'=>$faker->email,
        'msg'=>$faker->realText(50),
        'approve'=>$faker->randomElement(['true','false']),
    ];
});
