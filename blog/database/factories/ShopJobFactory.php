<?php

use Faker\Generator as Faker;

$factory->define(App\Models\ShopJob::class, function (Faker $faker) {
    return [
        'queue'=>$faker->unique()->randomDigit(1,50),
        'payload'=>$faker->realText(10),
        'payload'=>$faker->realText(10),
        'attempts'=>$faker->randomDigit(1,200),
        'reserved'=>$faker->randomDigit(1,200),
        'available_at'=>$faker->randomDigit(1,200),
    ];
});
