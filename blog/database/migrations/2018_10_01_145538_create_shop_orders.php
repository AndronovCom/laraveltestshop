<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_orders',function (Blueprint $table)
        {
            $table->increments('id');

            $table->string('delivery_city');
            $table->string('delivery_adr')->nullable();
            $table->string('delivery_np')->nullable();
            $table->enum('delivery_type',['adr','np'])->default('np');
            $table->enum('pay_type',['null','privat24','privat_terminal','liqpay'])->default('privat24');
            $table->string('code');
            $table->string('ttn')->nullable();
            $table->longText('comment')->nullable();
            $table->enum('status',['new','paid','sent'])->default('new');
            $table->timestamps();

            $table->integer('shop_client_id')->unsigned()->default(1);
            $table->foreign('shop_client_id')->references('id')->on('shop_clients');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_orders');

    }
}
