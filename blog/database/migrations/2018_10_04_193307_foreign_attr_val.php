<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignAttrVal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shop_attr_val', function (Blueprint $table) {
            $table->integer('shop_attr_id')->unsigned()->default(1);
            $table->foreign('shop_attr_id')->references('id')->on('shop_attr');

            $table->integer('shop_product_id')->unsigned()->default(1);
            $table->foreign('shop_product_id')->references('id')->on('shop_products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shop_attr_val', function (Blueprint $table) {
            $table->dropColumn('shop_attr_id');
            $table->dropColumn('shop_product_id');
        });
    }
}
