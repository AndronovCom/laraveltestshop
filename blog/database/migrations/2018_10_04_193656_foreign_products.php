<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shop_products', function (Blueprint $table) {
            $table->integer('category_id')->unsigned()->default(1);
            $table->foreign('category_id')->references('id')->on('shop_category');

            $table->integer('parent_id')->unsigned()->nullable();
            $table->foreign('parent_id')->references('id')->on('shop_products');

            $table->integer('shop_gallery_id')->unsigned()->nullable();
            $table->foreign('shop_gallery_id')->references('id')->on('shop_gallaries');

            $table->integer('shop_gallery_back_id')->unsigned()->nullable();
            $table->foreign('shop_gallery_back_id')->references('id')->on('shop_gallaries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shop_products', function (Blueprint $table) {
            $table->dropColumn('shop_attr_id');
            $table->dropColumn('parent_id');
            //
        });
    }
}
