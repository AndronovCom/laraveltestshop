<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopOrderFiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_order_files',function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('hash')->nullable();
            $table->string('mime')->nullable();
            $table->string('extension')->nullable();
            $table->enum('status',['tmp','success'])->default('tmp');
            $table->enum('image',['true','false'])->default('false');
            $table->timestamps();

            $table->integer('shop_order_id')->unsigned()->default(1);
            $table->foreign('shop_order_id')->references('id')->on('shop_orders');


        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_order_files');

    }
}
