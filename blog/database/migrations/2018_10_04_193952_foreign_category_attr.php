<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignCategoryAttr extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shop_category_attr', function (Blueprint $table)
        {
            $table->integer('shop_category_id')->unsigned()->default(1);
            $table->foreign('shop_category_id')->references('id')->on('shop_category');

            $table->integer('shop_attr_id')->unsigned()->default(1);
            $table->foreign('shop_attr_id')->references('id')->on('shop_attr');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shop_category_attr', function (Blueprint $table) {
            $table->dropColumn('shop_category_id');
            $table->dropColumn('shop_attr_id');

        });
    }
}
