<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateShopCategoryAttrTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('shop_category_attr', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('weigth')->nullable();
			$table->timestamps();
			$table->softDeletes();

            $table->integer('parent_id')->unsigned()->default(1);
            $table->foreign('parent_id')->references('id')->on('shop_category_attr');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('shop_category_attr');
	}

}
