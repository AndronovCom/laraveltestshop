<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_products', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('title');
            $table->string('name')->nullable();
            $table->string('keywords')->nullable();
            $table->text('description', 65535)->nullable();
            $table->text('description_full', 65535)->nullable();
            $table->string('values')->nullable();
            $table->string('cover')->nullable();
            $table->string('sku')->nullable();
            $table->float('price', 10, 0);
            $table->float('price_old', 10, 0)->nullable();
            $table->string('label')->nullable();
            $table->enum('isset',['true','false'])->default('true');
            $table->enum('visible',['true','false'])->default('true');
            $table->string('urlhash')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_product');
    }
}
